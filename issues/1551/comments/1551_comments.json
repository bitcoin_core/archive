[
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/6728608",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-6728608",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 6728608,
    "node_id": "MDEyOklzc3VlQ29tbWVudDY3Mjg2MDg=",
    "user": {
      "login": "sipa",
      "id": 548488,
      "node_id": "MDQ6VXNlcjU0ODQ4OA==",
      "avatar_url": "https://avatars.githubusercontent.com/u/548488?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/sipa",
      "html_url": "https://github.com/sipa",
      "followers_url": "https://api.github.com/users/sipa/followers",
      "following_url": "https://api.github.com/users/sipa/following{/other_user}",
      "gists_url": "https://api.github.com/users/sipa/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/sipa/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/sipa/subscriptions",
      "organizations_url": "https://api.github.com/users/sipa/orgs",
      "repos_url": "https://api.github.com/users/sipa/repos",
      "events_url": "https://api.github.com/users/sipa/events{/privacy}",
      "received_events_url": "https://api.github.com/users/sipa/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-07-03T08:22:07Z",
    "updated_at": "2012-07-03T08:22:07Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">Interesting. I definitely want shared locks at some point in the future.</p>\n<p dir=\"auto\">You seem to implement some own implementation for a recursive shared lock? That certainly needs a lot of testing, as it can easily have subtle bugs. Maybe it is easier to clean up the code so that we don't need recursive locking at all (if all data structures are properly encapsulated, you can put the taking of locks in all entry functions, and never further inside).</p>\n<p dir=\"auto\">Also, to get the best performance, you'll also need support for upgradable locks. For example, connecting a block has two stages, one where data is read from the backend (inputs fetched from db), and one where the changes are committed. If the first stage happens in an upgradable lock, it can happen concurrently with read-only locks (most RPC queries...), even while protecting that only a single (potential) writer is active.</p>",
    "body_text": "Interesting. I definitely want shared locks at some point in the future.\nYou seem to implement some own implementation for a recursive shared lock? That certainly needs a lot of testing, as it can easily have subtle bugs. Maybe it is easier to clean up the code so that we don't need recursive locking at all (if all data structures are properly encapsulated, you can put the taking of locks in all entry functions, and never further inside).\nAlso, to get the best performance, you'll also need support for upgradable locks. For example, connecting a block has two stages, one where data is read from the backend (inputs fetched from db), and one where the changes are committed. If the first stage happens in an upgradable lock, it can happen concurrently with read-only locks (most RPC queries...), even while protecting that only a single (potential) writer is active.",
    "body": "Interesting. I definitely want shared locks at some point in the future.\n\nYou seem to implement some own implementation for a recursive shared lock? That certainly needs a lot of testing, as it can easily have subtle bugs. Maybe it is easier to clean up the code so that we don't need recursive locking at all (if all data structures are properly encapsulated, you can put the taking of locks in all entry functions, and never further inside).\n\nAlso, to get the best performance, you'll also need support for upgradable locks. For example, connecting a block has two stages, one where data is read from the backend (inputs fetched from db), and one where the changes are committed. If the first stage happens in an upgradable lock, it can happen concurrently with read-only locks (most RPC queries...), even while protecting that only a single (potential) writer is active.\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/6728608/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/6735366",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-6735366",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 6735366,
    "node_id": "MDEyOklzc3VlQ29tbWVudDY3MzUzNjY=",
    "user": {
      "login": "TheBlueMatt",
      "id": 649246,
      "node_id": "MDQ6VXNlcjY0OTI0Ng==",
      "avatar_url": "https://avatars.githubusercontent.com/u/649246?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/TheBlueMatt",
      "html_url": "https://github.com/TheBlueMatt",
      "followers_url": "https://api.github.com/users/TheBlueMatt/followers",
      "following_url": "https://api.github.com/users/TheBlueMatt/following{/other_user}",
      "gists_url": "https://api.github.com/users/TheBlueMatt/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/TheBlueMatt/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/TheBlueMatt/subscriptions",
      "organizations_url": "https://api.github.com/users/TheBlueMatt/orgs",
      "repos_url": "https://api.github.com/users/TheBlueMatt/repos",
      "events_url": "https://api.github.com/users/TheBlueMatt/events{/privacy}",
      "received_events_url": "https://api.github.com/users/TheBlueMatt/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-07-03T14:15:01Z",
    "updated_at": "2012-07-03T14:15:01Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">Sadly, boost only provides recursive locking support in boost::recursive_mutex, not in boost::shared_mutex.  Thus I had to add recursive checking to CCriticalSection.  In terms of making bitcoin not need recursive locks...that would get very ugly.  Bitcoin is such a mess as it is, trying to track down all the cases where we depend on recursive locks, I would think, would be quite a bit of effort.  Yes something like this needs to be beaten on for a while before merge, but the code is pretty straightforward.  Also, Im still adding more test cases.</p>\n<p dir=\"auto\">Added upgrade lock.</p>",
    "body_text": "Sadly, boost only provides recursive locking support in boost::recursive_mutex, not in boost::shared_mutex.  Thus I had to add recursive checking to CCriticalSection.  In terms of making bitcoin not need recursive locks...that would get very ugly.  Bitcoin is such a mess as it is, trying to track down all the cases where we depend on recursive locks, I would think, would be quite a bit of effort.  Yes something like this needs to be beaten on for a while before merge, but the code is pretty straightforward.  Also, Im still adding more test cases.\nAdded upgrade lock.",
    "body": "Sadly, boost only provides recursive locking support in boost::recursive_mutex, not in boost::shared_mutex.  Thus I had to add recursive checking to CCriticalSection.  In terms of making bitcoin not need recursive locks...that would get very ugly.  Bitcoin is such a mess as it is, trying to track down all the cases where we depend on recursive locks, I would think, would be quite a bit of effort.  Yes something like this needs to be beaten on for a while before merge, but the code is pretty straightforward.  Also, Im still adding more test cases.\n\nAdded upgrade lock.\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/6735366/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/7613114",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-7613114",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 7613114,
    "node_id": "MDEyOklzc3VlQ29tbWVudDc2MTMxMTQ=",
    "user": {
      "login": "BitcoinPullTester",
      "id": 2110907,
      "node_id": "MDQ6VXNlcjIxMTA5MDc=",
      "avatar_url": "https://avatars.githubusercontent.com/u/2110907?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/BitcoinPullTester",
      "html_url": "https://github.com/BitcoinPullTester",
      "followers_url": "https://api.github.com/users/BitcoinPullTester/followers",
      "following_url": "https://api.github.com/users/BitcoinPullTester/following{/other_user}",
      "gists_url": "https://api.github.com/users/BitcoinPullTester/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/BitcoinPullTester/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/BitcoinPullTester/subscriptions",
      "organizations_url": "https://api.github.com/users/BitcoinPullTester/orgs",
      "repos_url": "https://api.github.com/users/BitcoinPullTester/repos",
      "events_url": "https://api.github.com/users/BitcoinPullTester/events{/privacy}",
      "received_events_url": "https://api.github.com/users/BitcoinPullTester/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-08-09T12:50:57Z",
    "updated_at": "2012-08-09T12:50:57Z",
    "author_association": "NONE",
    "body_html": "<p dir=\"auto\">Automatic sanity-testing: FAILED, see <a href=\"http://jenkins.bluematt.me/pull-tester/d3d8bf6f924678961c5f8200757f1deaa8630981\" rel=\"nofollow\">http://jenkins.bluematt.me/pull-tester/d3d8bf6f924678961c5f8200757f1deaa8630981</a> for binaries and test log.</p>\n<p dir=\"auto\">This could happen for one of several reasons:</p>\n<ol dir=\"auto\">\n<li>It chanages paths in makefile.linux-mingw or otherwise changes build scripts in a way that made them incompatible with the automated testing scripts</li>\n<li>It does not merge cleanly onto current master</li>\n<li>It does not build on either Linux i386 or Win32 (via MinGW cross compile)</li>\n<li>The test suite fails on either Linux i386 or Win32</li>\n</ol>",
    "body_text": "Automatic sanity-testing: FAILED, see http://jenkins.bluematt.me/pull-tester/d3d8bf6f924678961c5f8200757f1deaa8630981 for binaries and test log.\nThis could happen for one of several reasons:\n\nIt chanages paths in makefile.linux-mingw or otherwise changes build scripts in a way that made them incompatible with the automated testing scripts\nIt does not merge cleanly onto current master\nIt does not build on either Linux i386 or Win32 (via MinGW cross compile)\nThe test suite fails on either Linux i386 or Win32",
    "body": "Automatic sanity-testing: FAILED, see http://jenkins.bluematt.me/pull-tester/d3d8bf6f924678961c5f8200757f1deaa8630981 for binaries and test log.\n\nThis could happen for one of several reasons:\n1. It chanages paths in makefile.linux-mingw or otherwise changes build scripts in a way that made them incompatible with the automated testing scripts\n2. It does not merge cleanly onto current master\n3. It does not build on either Linux i386 or Win32 (via MinGW cross compile)\n4. The test suite fails on either Linux i386 or Win32\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/7613114/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8284685",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8284685",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8284685,
    "node_id": "MDEyOklzc3VlQ29tbWVudDgyODQ2ODU=",
    "user": {
      "login": "jgarzik",
      "id": 494411,
      "node_id": "MDQ6VXNlcjQ5NDQxMQ==",
      "avatar_url": "https://avatars.githubusercontent.com/u/494411?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jgarzik",
      "html_url": "https://github.com/jgarzik",
      "followers_url": "https://api.github.com/users/jgarzik/followers",
      "following_url": "https://api.github.com/users/jgarzik/following{/other_user}",
      "gists_url": "https://api.github.com/users/jgarzik/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jgarzik/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jgarzik/subscriptions",
      "organizations_url": "https://api.github.com/users/jgarzik/orgs",
      "repos_url": "https://api.github.com/users/jgarzik/repos",
      "events_url": "https://api.github.com/users/jgarzik/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jgarzik/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-05T01:12:26Z",
    "updated_at": "2012-09-05T01:12:26Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">Recommend closing, for now.  No ACKs gathered, and it seems to me like a <em>valid</em> tool for our toolbox, without an immediate demonstrated need.  Our locks do not seem highly contended, which seems to bump this quite down the priority scale down to \"theoretically useful.\"</p>",
    "body_text": "Recommend closing, for now.  No ACKs gathered, and it seems to me like a valid tool for our toolbox, without an immediate demonstrated need.  Our locks do not seem highly contended, which seems to bump this quite down the priority scale down to \"theoretically useful.\"",
    "body": "Recommend closing, for now.  No ACKs gathered, and it seems to me like a _valid_ tool for our toolbox, without an immediate demonstrated need.  Our locks do not seem highly contended, which seems to bump this quite down the priority scale down to \"theoretically useful.\"\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8284685/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8284759",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8284759",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8284759,
    "node_id": "MDEyOklzc3VlQ29tbWVudDgyODQ3NTk=",
    "user": {
      "login": "sipa",
      "id": 548488,
      "node_id": "MDQ6VXNlcjU0ODQ4OA==",
      "avatar_url": "https://avatars.githubusercontent.com/u/548488?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/sipa",
      "html_url": "https://github.com/sipa",
      "followers_url": "https://api.github.com/users/sipa/followers",
      "following_url": "https://api.github.com/users/sipa/following{/other_user}",
      "gists_url": "https://api.github.com/users/sipa/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/sipa/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/sipa/subscriptions",
      "organizations_url": "https://api.github.com/users/sipa/orgs",
      "repos_url": "https://api.github.com/users/sipa/repos",
      "events_url": "https://api.github.com/users/sipa/events{/privacy}",
      "received_events_url": "https://api.github.com/users/sipa/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-05T01:16:50Z",
    "updated_at": "2012-09-05T01:17:12Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\"><a class=\"user-mention notranslate\" data-hovercard-type=\"user\" data-hovercard-url=\"/users/jgarzik/hovercard\" data-octo-click=\"hovercard-link-click\" data-octo-dimensions=\"link_type:self\" href=\"https://github.com/jgarzik\">@jgarzik</a> Not contended? We have a cs_main that blocks anything useful being done in parallel. Since many tasks only need read-only access to data structures, shared locks could increase parallellism massively.</p>\n<p dir=\"auto\">That said, I still disagree with an implementation that releases a lock when trying to go from shared to exclusive.</p>",
    "body_text": "@jgarzik Not contended? We have a cs_main that blocks anything useful being done in parallel. Since many tasks only need read-only access to data structures, shared locks could increase parallellism massively.\nThat said, I still disagree with an implementation that releases a lock when trying to go from shared to exclusive.",
    "body": "@jgarzik Not contended? We have a cs_main that blocks anything useful being done in parallel. Since many tasks only need read-only access to data structures, shared locks could increase parallellism massively.\n\nThat said, I still disagree with an implementation that releases a lock when trying to go from shared to exclusive.\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8284759/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8285175",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8285175",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8285175,
    "node_id": "MDEyOklzc3VlQ29tbWVudDgyODUxNzU=",
    "user": {
      "login": "TheBlueMatt",
      "id": 649246,
      "node_id": "MDQ6VXNlcjY0OTI0Ng==",
      "avatar_url": "https://avatars.githubusercontent.com/u/649246?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/TheBlueMatt",
      "html_url": "https://github.com/TheBlueMatt",
      "followers_url": "https://api.github.com/users/TheBlueMatt/followers",
      "following_url": "https://api.github.com/users/TheBlueMatt/following{/other_user}",
      "gists_url": "https://api.github.com/users/TheBlueMatt/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/TheBlueMatt/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/TheBlueMatt/subscriptions",
      "organizations_url": "https://api.github.com/users/TheBlueMatt/orgs",
      "repos_url": "https://api.github.com/users/TheBlueMatt/repos",
      "events_url": "https://api.github.com/users/TheBlueMatt/events{/privacy}",
      "received_events_url": "https://api.github.com/users/TheBlueMatt/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-05T01:42:29Z",
    "updated_at": "2012-09-05T01:42:29Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">ACK on lack of ACKs</p>",
    "body_text": "ACK on lack of ACKs",
    "body": "ACK on lack of ACKs\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8285175/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8748568",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8748568",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8748568,
    "node_id": "MDEyOklzc3VlQ29tbWVudDg3NDg1Njg=",
    "user": {
      "login": "luke-jr",
      "id": 1095675,
      "node_id": "MDQ6VXNlcjEwOTU2NzU=",
      "avatar_url": "https://avatars.githubusercontent.com/u/1095675?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/luke-jr",
      "html_url": "https://github.com/luke-jr",
      "followers_url": "https://api.github.com/users/luke-jr/followers",
      "following_url": "https://api.github.com/users/luke-jr/following{/other_user}",
      "gists_url": "https://api.github.com/users/luke-jr/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/luke-jr/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/luke-jr/subscriptions",
      "organizations_url": "https://api.github.com/users/luke-jr/orgs",
      "repos_url": "https://api.github.com/users/luke-jr/repos",
      "events_url": "https://api.github.com/users/luke-jr/events{/privacy}",
      "received_events_url": "https://api.github.com/users/luke-jr/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-20T22:56:46Z",
    "updated_at": "2012-09-20T22:56:46Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">FWIW, the new tests in this seem to fail occasionally.</p>",
    "body_text": "FWIW, the new tests in this seem to fail occasionally.",
    "body": "FWIW, the new tests in this seem to fail occasionally.\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8748568/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8748698",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8748698",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8748698,
    "node_id": "MDEyOklzc3VlQ29tbWVudDg3NDg2OTg=",
    "user": {
      "login": "TheBlueMatt",
      "id": 649246,
      "node_id": "MDQ6VXNlcjY0OTI0Ng==",
      "avatar_url": "https://avatars.githubusercontent.com/u/649246?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/TheBlueMatt",
      "html_url": "https://github.com/TheBlueMatt",
      "followers_url": "https://api.github.com/users/TheBlueMatt/followers",
      "following_url": "https://api.github.com/users/TheBlueMatt/following{/other_user}",
      "gists_url": "https://api.github.com/users/TheBlueMatt/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/TheBlueMatt/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/TheBlueMatt/subscriptions",
      "organizations_url": "https://api.github.com/users/TheBlueMatt/orgs",
      "repos_url": "https://api.github.com/users/TheBlueMatt/repos",
      "events_url": "https://api.github.com/users/TheBlueMatt/events{/privacy}",
      "received_events_url": "https://api.github.com/users/TheBlueMatt/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-20T23:02:44Z",
    "updated_at": "2012-09-20T23:02:44Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">Yea, it didnt work 100%, but I never got around to debugging it, and it was never a high priority (no interest anyway...)</p>",
    "body_text": "Yea, it didnt work 100%, but I never got around to debugging it, and it was never a high priority (no interest anyway...)",
    "body": "Yea, it didnt work 100%, but I never got around to debugging it, and it was never a high priority (no interest anyway...)\n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8748698/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8762141",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/1551#issuecomment-8762141",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/1551",
    "id": 8762141,
    "node_id": "MDEyOklzc3VlQ29tbWVudDg3NjIxNDE=",
    "user": {
      "login": "laanwj",
      "id": 126646,
      "node_id": "MDQ6VXNlcjEyNjY0Ng==",
      "avatar_url": "https://avatars.githubusercontent.com/u/126646?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/laanwj",
      "html_url": "https://github.com/laanwj",
      "followers_url": "https://api.github.com/users/laanwj/followers",
      "following_url": "https://api.github.com/users/laanwj/following{/other_user}",
      "gists_url": "https://api.github.com/users/laanwj/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/laanwj/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/laanwj/subscriptions",
      "organizations_url": "https://api.github.com/users/laanwj/orgs",
      "repos_url": "https://api.github.com/users/laanwj/repos",
      "events_url": "https://api.github.com/users/laanwj/events{/privacy}",
      "received_events_url": "https://api.github.com/users/laanwj/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2012-09-21T11:47:46Z",
    "updated_at": "2012-09-21T11:47:46Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">This is a nice idea and will allow for some extra concurrency, but it makes reasoning in the current mess that is bitcoin locking even harder.</p>\n<p dir=\"auto\">I agree with sipa that we first need to get rid of recursive locks (and in the meantime, get getter insight into the current mess), before introducing  more complex locking primitives.</p>",
    "body_text": "This is a nice idea and will allow for some extra concurrency, but it makes reasoning in the current mess that is bitcoin locking even harder.\nI agree with sipa that we first need to get rid of recursive locks (and in the meantime, get getter insight into the current mess), before introducing  more complex locking primitives.",
    "body": "This is a nice idea and will allow for some extra concurrency, but it makes reasoning in the current mess that is bitcoin locking even harder.\n\nI agree with sipa that we first need to get rid of recursive locks (and in the meantime, get getter insight into the current mess), before introducing  more complex locking primitives. \n",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/8762141/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  }
]
