{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/23832",
  "id": 1085765463,
  "node_id": "PR_kwDOABII584wISCX",
  "number": 23832,
  "title": "Refactor: Changes time variables from int to chrono",
  "user": {
    "login": "shaavan",
    "id": 85434418,
    "node_id": "MDQ6VXNlcjg1NDM0NDE4",
    "avatar_url": "https://avatars.githubusercontent.com/u/85434418?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/shaavan",
    "html_url": "https://github.com/shaavan",
    "followers_url": "https://api.github.com/users/shaavan/followers",
    "following_url": "https://api.github.com/users/shaavan/following{/other_user}",
    "gists_url": "https://api.github.com/users/shaavan/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/shaavan/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/shaavan/subscriptions",
    "organizations_url": "https://api.github.com/users/shaavan/orgs",
    "repos_url": "https://api.github.com/users/shaavan/repos",
    "events_url": "https://api.github.com/users/shaavan/events{/privacy}",
    "received_events_url": "https://api.github.com/users/shaavan/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 135961,
      "node_id": "MDU6TGFiZWwxMzU5NjE=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Refactoring",
      "name": "Refactoring",
      "color": "E6F6D6",
      "default": false,
      "description": null
    },
    {
      "id": 98298007,
      "node_id": "MDU6TGFiZWw5ODI5ODAwNw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/P2P",
      "name": "P2P",
      "color": "006b75",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 5,
  "created_at": "2021-12-21T12:08:51Z",
  "updated_at": "2023-01-06T10:06:44Z",
  "closed_at": "2022-01-06T12:39:21Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/23832",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/23832",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/23832.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/23832.patch",
    "merged_at": "2022-01-06T12:39:21Z"
  },
  "body_html": "<p dir=\"auto\">This PR is a follow-up to <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1083188412\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/23801\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/23801/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/23801\">#23801</a>.<br>\nThis PR aims to make the following changes to all the time variables in <strong>net_processing.cpp</strong> wherever possible.</p>\n<ul dir=\"auto\">\n<li>Convert all time variables to <code class=\"notranslate\">std::chrono.</code></li>\n<li>Use <code class=\"notranslate\">chorno::literals</code> wherever possible.</li>\n<li>Use <code class=\"notranslate\">auto</code> keywords wherever possible.</li>\n<li>Use <code class=\"notranslate\">var{val}</code> convention of initialization.</li>\n</ul>\n<p dir=\"auto\">This PR also minimizes the number of times, serialization of time <code class=\"notranslate\">count_seconds(..)</code> occurs.</p>",
  "body_text": "This PR is a follow-up to #23801.\nThis PR aims to make the following changes to all the time variables in net_processing.cpp wherever possible.\n\nConvert all time variables to std::chrono.\nUse chorno::literals wherever possible.\nUse auto keywords wherever possible.\nUse var{val} convention of initialization.\n\nThis PR also minimizes the number of times, serialization of time count_seconds(..) occurs.",
  "body": "This PR is a follow-up to #23801.\r\nThis PR aims to make the following changes to all the time variables in **net_processing.cpp** wherever possible.\r\n\r\n- Convert all time variables to `std::chrono.`\r\n- Use `chorno::literals` wherever possible.\r\n- Use `auto` keywords wherever possible.\r\n- Use `var{val}` convention of initialization.\r\n\r\nThis PR also minimizes the number of times, serialization of time `count_seconds(..)` occurs.\r\n",
  "closed_by": {
    "login": "maflcko",
    "id": 6399679,
    "node_id": "MDQ6VXNlcjYzOTk2Nzk=",
    "avatar_url": "https://avatars.githubusercontent.com/u/6399679?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/maflcko",
    "html_url": "https://github.com/maflcko",
    "followers_url": "https://api.github.com/users/maflcko/followers",
    "following_url": "https://api.github.com/users/maflcko/following{/other_user}",
    "gists_url": "https://api.github.com/users/maflcko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/maflcko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/maflcko/subscriptions",
    "organizations_url": "https://api.github.com/users/maflcko/orgs",
    "repos_url": "https://api.github.com/users/maflcko/repos",
    "events_url": "https://api.github.com/users/maflcko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/maflcko/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23832/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
