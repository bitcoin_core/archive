{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/22041",
  "id": 899568551,
  "node_id": "MDExOlB1bGxSZXF1ZXN0NjUxMjUyODg5",
  "number": 22041,
  "title": "build: Make --enable-suppress-external-warnings the default ",
  "user": {
    "login": "hebasto",
    "id": 32963518,
    "node_id": "MDQ6VXNlcjMyOTYzNTE4",
    "avatar_url": "https://avatars.githubusercontent.com/u/32963518?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/hebasto",
    "html_url": "https://github.com/hebasto",
    "followers_url": "https://api.github.com/users/hebasto/followers",
    "following_url": "https://api.github.com/users/hebasto/following{/other_user}",
    "gists_url": "https://api.github.com/users/hebasto/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/hebasto/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/hebasto/subscriptions",
    "organizations_url": "https://api.github.com/users/hebasto/orgs",
    "repos_url": "https://api.github.com/users/hebasto/repos",
    "events_url": "https://api.github.com/users/hebasto/events{/privacy}",
    "received_events_url": "https://api.github.com/users/hebasto/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 61889416,
      "node_id": "MDU6TGFiZWw2MTg4OTQxNg==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Build%20system",
      "name": "Build system",
      "color": "5319e7",
      "default": false,
      "description": null
    },
    {
      "id": 955867938,
      "node_id": "MDU6TGFiZWw5NTU4Njc5Mzg=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Needs%20rebase",
      "name": "Needs rebase",
      "color": "cccccc",
      "default": false,
      "description": ""
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 9,
  "created_at": "2021-05-24T11:20:04Z",
  "updated_at": "2023-03-28T10:21:26Z",
  "closed_at": "2021-11-21T19:51:41Z",
  "author_association": "MEMBER",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/22041",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/22041",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/22041.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/22041.patch",
    "merged_at": null
  },
  "body_html": "<p dir=\"auto\">This is split from <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"857011742\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/21667\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/21667/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/21667\">#21667</a>.</p>\n<p dir=\"auto\">For users, one of the preferred ways to use the Bitcoin Core client is compiling from source, either downloaded as a part of release or acquired via git.</p>\n<p dir=\"auto\">Only basic knowledge about terminal is required to run:</p>\n<div class=\"snippet-clipboard-content notranslate position-relative overflow-auto\" data-snippet-clipboard-copy-content=\"$ cd bitcoin\n$ ./autogen.sh\n$ ./configure\n$ make check\"><pre class=\"notranslate\"><code class=\"notranslate\">$ cd bitcoin\n$ ./autogen.sh\n$ ./configure\n$ make check\n</code></pre></div>\n<p dir=\"auto\">Also, it is natural to expect that number of users that have such knowledge is higher than the number of advanced users or developers.</p>\n<p dir=\"auto\">Assuming the basic knowledge level of users it seems unwise to allow compiler and linker warnings because they could undermine the users' trust in the Bitcoin Core quality/security/reliability.</p>\n<p dir=\"auto\">Of course, warnings could be emitted by different parts of code: either the code we are responsible for, or third parties code, including sub-trees in our repo:</p>\n<ul dir=\"auto\">\n<li><a href=\"https://github.com/google/crc32c\">https://github.com/google/crc32c</a></li>\n<li><a href=\"https://github.com/google/leveldb\">https://github.com/google/leveldb</a></li>\n</ul>\n<p dir=\"auto\">and other dependencies:</p>\n<ul dir=\"auto\">\n<li><a href=\"https://github.com/libevent/libevent\">https://github.com/libevent/libevent</a></li>\n<li><a href=\"https://github.com/boostorg/boost\">https://github.com/boostorg/boost</a></li>\n<li><a href=\"https://github.com/qt/qtbase\">https://github.com/qt/qtbase</a></li>\n<li>...</li>\n</ul>\n<p dir=\"auto\">Currently, warnings from <code class=\"notranslate\">leveldb</code> are suppressed <em>unconditionally</em>: </p><div class=\"Box Box--condensed my-2\">\n  <div class=\"Box-header f6\">\n    <p class=\"mb-0 text-bold\">\n      <a href=\"https://github.com/bitcoin/bitcoin/blob/599000903e09e5ae3784d15ae078a2e1ed89ab12/src/Makefile.leveldb.include#L39\">bitcoin/src/Makefile.leveldb.include</a>\n    </p>\n    <p class=\"mb-0 color-fg-muted\">\n         Line 39\n      in\n      <a data-pjax=\"true\" class=\"commit-tease-sha Link--inTextBlock\" href=\"/bitcoin/bitcoin/commit/599000903e09e5ae3784d15ae078a2e1ed89ab12\">5990009</a>\n    </p>\n  </div>\n  <div itemprop=\"text\" class=\"Box-body p-0 blob-wrapper blob-wrapper-embedded data\">\n    <table class=\"highlight tab-size mb-0 js-file-line-container\" data-tab-size=\"8\" data-paste-markdown-skip=\"\">\n\n        <tbody><tr class=\"border-0\">\n          <td id=\"L39\" class=\"blob-num border-0 px-3 py-0 color-bg-default\" data-line-number=\"39\"></td>\n          <td id=\"LC39\" class=\"blob-code border-0 px-3 py-0 color-bg-default blob-code-inner js-file-line\"> leveldb_libleveldb_a_CXXFLAGS = $(filter-out -Wconditional-uninitialized -Werror=conditional-uninitialized -Wsuggest-override -Werror=suggest-override, $(AM_CXXFLAGS)) $(PIE_FLAGS) </td>\n        </tr>\n    </tbody></table>\n  </div>\n</div>\n<p></p>\n<p dir=\"auto\">OTOH, warnings from Qt and Boost code could be suppressed with the <code class=\"notranslate\">--enable-suppress-external-warnings</code> configure option.</p>\n<p dir=\"auto\">Making the <code class=\"notranslate\">--enable-suppress-external-warnings</code> the default has the following benefits:</p>\n<ul dir=\"auto\">\n<li>don't alarm users who compile from source</li>\n<li>increase signal/noise ratio for developers when a new warning appears in our code (as an example, Qt warnings on macOS, where Homebrew's Qt is pretty fresh)</li>\n<li>allows to apply more <code class=\"notranslate\">-W</code> and <code class=\"notranslate\">-Werror</code> options by default (for example, <code class=\"notranslate\">-Wdocumentation</code> and <code class=\"notranslate\">-Werror=documentation</code> introduced in <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"851074338\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/21613\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/21613/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/21613\">#21613</a>)</li>\n</ul>\n<p dir=\"auto\">There was an <a href=\"https://github.com/bitcoin/bitcoin/pull/21667#issuecomment-820185228\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/21667/hovercard\">objection</a>:</p>\n<blockquote>\n<p dir=\"auto\">I don't think we should make <code class=\"notranslate\">--enable-suppress-external-warnings</code> the default, but it should be better documented. Someone needs to be motivated to fix things upstream and/or notice problems when we update dependencies.</p>\n</blockquote>\n<p dir=\"auto\">The answer to this objection is that fixing \"things upstream\" is not the top priority for developers. It is always possible to build with the <code class=\"notranslate\">--disable-suppress-external-warnings</code>. Even more, with this PR the <code class=\"notranslate\">--disable-suppress-external-warnings</code> will reveal <code class=\"notranslate\">leveldb</code> warning as well.</p>",
  "body_text": "This is split from #21667.\nFor users, one of the preferred ways to use the Bitcoin Core client is compiling from source, either downloaded as a part of release or acquired via git.\nOnly basic knowledge about terminal is required to run:\n$ cd bitcoin\n$ ./autogen.sh\n$ ./configure\n$ make check\n\nAlso, it is natural to expect that number of users that have such knowledge is higher than the number of advanced users or developers.\nAssuming the basic knowledge level of users it seems unwise to allow compiler and linker warnings because they could undermine the users' trust in the Bitcoin Core quality/security/reliability.\nOf course, warnings could be emitted by different parts of code: either the code we are responsible for, or third parties code, including sub-trees in our repo:\n\nhttps://github.com/google/crc32c\nhttps://github.com/google/leveldb\n\nand other dependencies:\n\nhttps://github.com/libevent/libevent\nhttps://github.com/boostorg/boost\nhttps://github.com/qt/qtbase\n...\n\nCurrently, warnings from leveldb are suppressed unconditionally: \n  \n    \n      bitcoin/src/Makefile.leveldb.include\n    \n    \n         Line 39\n      in\n      5990009\n    \n  \n  \n    \n\n        \n          \n           leveldb_libleveldb_a_CXXFLAGS = $(filter-out -Wconditional-uninitialized -Werror=conditional-uninitialized -Wsuggest-override -Werror=suggest-override, $(AM_CXXFLAGS)) $(PIE_FLAGS) \n        \n    \n  \n\n\nOTOH, warnings from Qt and Boost code could be suppressed with the --enable-suppress-external-warnings configure option.\nMaking the --enable-suppress-external-warnings the default has the following benefits:\n\ndon't alarm users who compile from source\nincrease signal/noise ratio for developers when a new warning appears in our code (as an example, Qt warnings on macOS, where Homebrew's Qt is pretty fresh)\nallows to apply more -W and -Werror options by default (for example, -Wdocumentation and -Werror=documentation introduced in #21613)\n\nThere was an objection:\n\nI don't think we should make --enable-suppress-external-warnings the default, but it should be better documented. Someone needs to be motivated to fix things upstream and/or notice problems when we update dependencies.\n\nThe answer to this objection is that fixing \"things upstream\" is not the top priority for developers. It is always possible to build with the --disable-suppress-external-warnings. Even more, with this PR the --disable-suppress-external-warnings will reveal leveldb warning as well.",
  "body": "This is split from #21667.\r\n\r\nFor users, one of the preferred ways to use the Bitcoin Core client is compiling from source, either downloaded as a part of release or acquired via git.\r\n\r\nOnly basic knowledge about terminal is required to run:\r\n```\r\n$ cd bitcoin\r\n$ ./autogen.sh\r\n$ ./configure\r\n$ make check\r\n```\r\n\r\nAlso, it is natural to expect that number of users that have such knowledge is higher than the number of advanced users or developers.\r\n\r\nAssuming the basic knowledge level of users it seems unwise to allow compiler and linker warnings because they could undermine the users' trust in the Bitcoin Core quality/security/reliability.\r\n\r\nOf course, warnings could be emitted by different parts of code: either the code we are responsible for, or third parties code, including sub-trees in our repo:\r\n- https://github.com/google/crc32c\r\n- https://github.com/google/leveldb\r\n\r\nand other dependencies:\r\n- https://github.com/libevent/libevent\r\n- https://github.com/boostorg/boost\r\n- https://github.com/qt/qtbase\r\n- ...\r\n\r\nCurrently, warnings from `leveldb` are suppressed _unconditionally_: https://github.com/bitcoin/bitcoin/blob/599000903e09e5ae3784d15ae078a2e1ed89ab12/src/Makefile.leveldb.include#L39\r\n\r\nOTOH, warnings from Qt and Boost code could be suppressed with the `--enable-suppress-external-warnings` configure option.\r\n\r\nMaking the `--enable-suppress-external-warnings` the default has the following benefits:\r\n- don't alarm users who compile from source\r\n- increase signal/noise ratio for developers when a new warning appears in our code (as an example, Qt warnings on macOS, where Homebrew's Qt is pretty fresh)\r\n- allows to apply more `-W` and `-Werror` options by default (for example, `-Wdocumentation` and `-Werror=documentation` introduced in #21613)\r\n\r\nThere was an [objection](https://github.com/bitcoin/bitcoin/pull/21667#issuecomment-820185228): \r\n> I don't think we should make `--enable-suppress-external-warnings` the default, but it should be better documented. Someone needs to be motivated to fix things upstream and/or notice problems when we update dependencies.\r\n\r\nThe answer to this objection is that fixing \"things upstream\" is not the top priority for developers. It is always possible to build with the `--disable-suppress-external-warnings`. Even more, with this PR the `--disable-suppress-external-warnings` will reveal `leveldb` warning as well.",
  "closed_by": {
    "login": "hebasto",
    "id": 32963518,
    "node_id": "MDQ6VXNlcjMyOTYzNTE4",
    "avatar_url": "https://avatars.githubusercontent.com/u/32963518?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/hebasto",
    "html_url": "https://github.com/hebasto",
    "followers_url": "https://api.github.com/users/hebasto/followers",
    "following_url": "https://api.github.com/users/hebasto/following{/other_user}",
    "gists_url": "https://api.github.com/users/hebasto/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/hebasto/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/hebasto/subscriptions",
    "organizations_url": "https://api.github.com/users/hebasto/orgs",
    "repos_url": "https://api.github.com/users/hebasto/repos",
    "events_url": "https://api.github.com/users/hebasto/events{/privacy}",
    "received_events_url": "https://api.github.com/users/hebasto/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22041/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
