[
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2075118310",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/29949#issuecomment-2075118310",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/29949",
    "id": 2075118310,
    "node_id": "IC_kwDOABII5857r8rm",
    "user": {
      "login": "achow101",
      "id": 3782274,
      "node_id": "MDQ6VXNlcjM3ODIyNzQ=",
      "avatar_url": "https://avatars.githubusercontent.com/u/3782274?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/achow101",
      "html_url": "https://github.com/achow101",
      "followers_url": "https://api.github.com/users/achow101/followers",
      "following_url": "https://api.github.com/users/achow101/following{/other_user}",
      "gists_url": "https://api.github.com/users/achow101/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/achow101/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/achow101/subscriptions",
      "organizations_url": "https://api.github.com/users/achow101/orgs",
      "repos_url": "https://api.github.com/users/achow101/repos",
      "events_url": "https://api.github.com/users/achow101/events{/privacy}",
      "received_events_url": "https://api.github.com/users/achow101/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2024-04-24T14:42:23Z",
    "updated_at": "2024-04-24T14:42:23Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">Bitcoin Core cannot sign non-standard scripts or scripts that are not Miniscript. This is expected behavior. <code class=\"notranslate\">walletprocesspsbt</code> does not return an error because signing is not the only thing that it does, so returning an error for a failing to sign is not appropriate.</p>",
    "body_text": "Bitcoin Core cannot sign non-standard scripts or scripts that are not Miniscript. This is expected behavior. walletprocesspsbt does not return an error because signing is not the only thing that it does, so returning an error for a failing to sign is not appropriate.",
    "body": "Bitcoin Core cannot sign non-standard scripts or scripts that are not Miniscript. This is expected behavior. `walletprocesspsbt` does not return an error because signing is not the only thing that it does, so returning an error for a failing to sign is not appropriate.",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2075118310/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2075135656",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/29949#issuecomment-2075135656",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/29949",
    "id": 2075135656,
    "node_id": "IC_kwDOABII5857sA6o",
    "user": {
      "login": "sipa",
      "id": 548488,
      "node_id": "MDQ6VXNlcjU0ODQ4OA==",
      "avatar_url": "https://avatars.githubusercontent.com/u/548488?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/sipa",
      "html_url": "https://github.com/sipa",
      "followers_url": "https://api.github.com/users/sipa/followers",
      "following_url": "https://api.github.com/users/sipa/following{/other_user}",
      "gists_url": "https://api.github.com/users/sipa/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/sipa/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/sipa/subscriptions",
      "organizations_url": "https://api.github.com/users/sipa/orgs",
      "repos_url": "https://api.github.com/users/sipa/repos",
      "events_url": "https://api.github.com/users/sipa/events{/privacy}",
      "received_events_url": "https://api.github.com/users/sipa/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2024-04-24T14:50:28Z",
    "updated_at": "2024-04-24T14:50:28Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">For some more background, Bitcoin Core has logic to try various things when signing, which includes Miniscript. The script variant you tried with <code class=\"notranslate\">OP_NUMEQUAL</code> at the end is valid Miniscript (<code class=\"notranslate\">multi_a(2,&lt;key1&gt;,&lt;key2&gt;,&lt;key3&gt;)</code> specifically), but the variant with <code class=\"notranslate\">OP_GREATERTHANOREQUAL</code> is not.</p>\n<p dir=\"auto\">When signing fails, Bitcoin Core doesn't know why it failed; it simply didn't find any patterns it knew how to sign for, and as <a class=\"user-mention notranslate\" data-hovercard-type=\"user\" data-hovercard-url=\"/users/achow101/hovercard\" data-octo-click=\"hovercard-link-click\" data-octo-dimensions=\"link_type:self\" href=\"https://github.com/achow101\">@achow101</a> points out, it doesn't even know your intent was to sign.</p>",
    "body_text": "For some more background, Bitcoin Core has logic to try various things when signing, which includes Miniscript. The script variant you tried with OP_NUMEQUAL at the end is valid Miniscript (multi_a(2,<key1>,<key2>,<key3>) specifically), but the variant with OP_GREATERTHANOREQUAL is not.\nWhen signing fails, Bitcoin Core doesn't know why it failed; it simply didn't find any patterns it knew how to sign for, and as @achow101 points out, it doesn't even know your intent was to sign.",
    "body": "For some more background, Bitcoin Core has logic to try various things when signing, which includes Miniscript. The script variant you tried with `OP_NUMEQUAL` at the end is valid Miniscript (`multi_a(2,<key1>,<key2>,<key3>)` specifically), but the variant with `OP_GREATERTHANOREQUAL` is not.\r\n\r\nWhen signing fails, Bitcoin Core doesn't know why it failed; it simply didn't find any patterns it knew how to sign for, and as @achow101 points out, it doesn't even know your intent was to sign.",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2075135656/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2076276040",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/29949#issuecomment-2076276040",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/29949",
    "id": 2076276040,
    "node_id": "IC_kwDOABII5857wXVI",
    "user": {
      "login": "KonradStaniec",
      "id": 10688236,
      "node_id": "MDQ6VXNlcjEwNjg4MjM2",
      "avatar_url": "https://avatars.githubusercontent.com/u/10688236?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/KonradStaniec",
      "html_url": "https://github.com/KonradStaniec",
      "followers_url": "https://api.github.com/users/KonradStaniec/followers",
      "following_url": "https://api.github.com/users/KonradStaniec/following{/other_user}",
      "gists_url": "https://api.github.com/users/KonradStaniec/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/KonradStaniec/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/KonradStaniec/subscriptions",
      "organizations_url": "https://api.github.com/users/KonradStaniec/orgs",
      "repos_url": "https://api.github.com/users/KonradStaniec/repos",
      "events_url": "https://api.github.com/users/KonradStaniec/events{/privacy}",
      "received_events_url": "https://api.github.com/users/KonradStaniec/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2024-04-25T03:16:08Z",
    "updated_at": "2024-04-25T11:05:42Z",
    "author_association": "NONE",
    "body_html": "<p dir=\"auto\">Thanks for the quick responses! I think this issue can be closed then.</p>\n<p dir=\"auto\">Two follow up questions, I would have:</p>\n<ol dir=\"auto\">\n<li>Is there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?</li>\n<li>Is there some problem with <code class=\"notranslate\">OP_GREATERTHANOREQUAL</code>  which makes it non compatible with Miniscript ?</li>\n</ol>",
    "body_text": "Thanks for the quick responses! I think this issue can be closed then.\nTwo follow up questions, I would have:\n\nIs there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?\nIs there some problem with OP_GREATERTHANOREQUAL  which makes it non compatible with Miniscript ?",
    "body": "Thanks for the quick responses! I think this issue can be closed then.\r\n\r\nTwo follow up questions, I would have:\r\n1. Is there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?\r\n2. Is there some problem with `OP_GREATERTHANOREQUAL`  which makes it non compatible with Miniscript ?",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2076276040/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2077274021",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/29949#issuecomment-2077274021",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/29949",
    "id": 2077274021,
    "node_id": "IC_kwDOABII58570K-l",
    "user": {
      "login": "achow101",
      "id": 3782274,
      "node_id": "MDQ6VXNlcjM3ODIyNzQ=",
      "avatar_url": "https://avatars.githubusercontent.com/u/3782274?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/achow101",
      "html_url": "https://github.com/achow101",
      "followers_url": "https://api.github.com/users/achow101/followers",
      "following_url": "https://api.github.com/users/achow101/following{/other_user}",
      "gists_url": "https://api.github.com/users/achow101/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/achow101/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/achow101/subscriptions",
      "organizations_url": "https://api.github.com/users/achow101/orgs",
      "repos_url": "https://api.github.com/users/achow101/repos",
      "events_url": "https://api.github.com/users/achow101/events{/privacy}",
      "received_events_url": "https://api.github.com/users/achow101/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2024-04-25T14:02:05Z",
    "updated_at": "2024-04-25T14:02:05Z",
    "author_association": "MEMBER",
    "body_html": "<blockquote>\n<p dir=\"auto\">Is there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?</p>\n</blockquote>\n<p dir=\"auto\">No</p>\n<blockquote>\n<p dir=\"auto\">Is there some problem with <code class=\"notranslate\">OP_GREATERTHANOREQUAL</code> which makes it non compatible with Miniscript ?</p>\n</blockquote>\n<p dir=\"auto\">Yes, it is third party malleable, which makes it much harder to do analysis. For any additional signatures provided after the threshold has been reached, a third party can trivially remove those signatures and the transaction is still valid. Miniscript tries to not be third party malleable, and this script construction is trivially malleable, so it is not compatible.</p>",
    "body_text": "Is there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?\n\nNo\n\nIs there some problem with OP_GREATERTHANOREQUAL which makes it non compatible with Miniscript ?\n\nYes, it is third party malleable, which makes it much harder to do analysis. For any additional signatures provided after the threshold has been reached, a third party can trivially remove those signatures and the transaction is still valid. Miniscript tries to not be third party malleable, and this script construction is trivially malleable, so it is not compatible.",
    "body": "> Is there some other way to sign transaction, through bitcoind, which tries to spend from script which is not compatible with miniscript ?\r\n\r\nNo\r\n\r\n> Is there some problem with `OP_GREATERTHANOREQUAL` which makes it non compatible with Miniscript ?\r\n\r\nYes, it is third party malleable, which makes it much harder to do analysis. For any additional signatures provided after the threshold has been reached, a third party can trivially remove those signatures and the transaction is still valid. Miniscript tries to not be third party malleable, and this script construction is trivially malleable, so it is not compatible.",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/2077274021/reactions",
      "total_count": 1,
      "+1": 1,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  }
]
