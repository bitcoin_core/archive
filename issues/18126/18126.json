{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/18126",
  "id": 564051195,
  "node_id": "MDExOlB1bGxSZXF1ZXN0Mzc0MzUwNjIz",
  "number": 18126,
  "title": "tests: Add fuzzing harness testing the locale independence of the strencodings.h functions",
  "user": {
    "login": "practicalswift",
    "id": 7826565,
    "node_id": "MDQ6VXNlcjc4MjY1NjU=",
    "avatar_url": "https://avatars.githubusercontent.com/u/7826565?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/practicalswift",
    "html_url": "https://github.com/practicalswift",
    "followers_url": "https://api.github.com/users/practicalswift/followers",
    "following_url": "https://api.github.com/users/practicalswift/following{/other_user}",
    "gists_url": "https://api.github.com/users/practicalswift/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/practicalswift/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/practicalswift/subscriptions",
    "organizations_url": "https://api.github.com/users/practicalswift/orgs",
    "repos_url": "https://api.github.com/users/practicalswift/repos",
    "events_url": "https://api.github.com/users/practicalswift/events{/privacy}",
    "received_events_url": "https://api.github.com/users/practicalswift/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 62963516,
      "node_id": "MDU6TGFiZWw2Mjk2MzUxNg==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Tests",
      "name": "Tests",
      "color": "d4c5f9",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 5,
  "created_at": "2020-02-12T14:50:19Z",
  "updated_at": "2022-08-18T18:22:26Z",
  "closed_at": "2020-03-06T19:32:24Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": "resolved",
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/18126",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/18126",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/18126.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/18126.patch",
    "merged_at": "2020-03-06T19:32:24Z"
  },
  "body_html": "<p dir=\"auto\">Context: <a href=\"https://github.com/bitcoin/bitcoin/pull/18124\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/18124/hovercard\">C and C++ locale assumptions in bitcoind and bitcoin-qt</a></p>\n<p dir=\"auto\">Add fuzzing harness for locale independence testing of functions in <code class=\"notranslate\">strencodings.h</code> and <code class=\"notranslate\">tinyformat.h</code>.</p>\n<p dir=\"auto\">Test this PR using:</p>\n<div class=\"snippet-clipboard-content notranslate position-relative overflow-auto\" data-snippet-clipboard-copy-content=\"$ make distclean\n$ ./autogen.sh\n$ CC=clang CXX=clang++ ./configure --enable-fuzz \\\n      --with-sanitizers=address,fuzzer,undefined\n$ make\n$ src/test/fuzz/locale\n…\"><pre class=\"notranslate\"><code class=\"notranslate\">$ make distclean\n$ ./autogen.sh\n$ CC=clang CXX=clang++ ./configure --enable-fuzz \\\n      --with-sanitizers=address,fuzzer,undefined\n$ make\n$ src/test/fuzz/locale\n…\n</code></pre></div>\n<p dir=\"auto\">The tested functions (<code class=\"notranslate\">ParseInt32(…)</code>, <code class=\"notranslate\">ParseInt64(…)</code>, <code class=\"notranslate\">atoi(const std::string&amp;)</code>, <code class=\"notranslate\">atoi64(const std::string&amp; str)</code>, <code class=\"notranslate\">i64tostr(const char*)</code>, <code class=\"notranslate\">itostr(…)</code>, <code class=\"notranslate\">strprintf(…)</code>) all call locale dependent functions (such as <code class=\"notranslate\">strtol(…)</code>, <code class=\"notranslate\">strtoll(…)</code>, <code class=\"notranslate\">atoi(const char*)</code>, etc.) but are assumed to do so in a way that the tested functions return same results regardless of the chosen C locale (<code class=\"notranslate\">setlocale</code>).</p>\n<p dir=\"auto\">This fuzzer aims to test that those assumptions hold up also in practice now and over time.</p>",
  "body_text": "Context: C and C++ locale assumptions in bitcoind and bitcoin-qt\nAdd fuzzing harness for locale independence testing of functions in strencodings.h and tinyformat.h.\nTest this PR using:\n$ make distclean\n$ ./autogen.sh\n$ CC=clang CXX=clang++ ./configure --enable-fuzz \\\n      --with-sanitizers=address,fuzzer,undefined\n$ make\n$ src/test/fuzz/locale\n…\n\nThe tested functions (ParseInt32(…), ParseInt64(…), atoi(const std::string&), atoi64(const std::string& str), i64tostr(const char*), itostr(…), strprintf(…)) all call locale dependent functions (such as strtol(…), strtoll(…), atoi(const char*), etc.) but are assumed to do so in a way that the tested functions return same results regardless of the chosen C locale (setlocale).\nThis fuzzer aims to test that those assumptions hold up also in practice now and over time.",
  "body": "Context: [C and C++ locale assumptions in bitcoind and bitcoin-qt](https://github.com/bitcoin/bitcoin/pull/18124)\r\n\r\nAdd fuzzing harness for locale independence testing of functions in `strencodings.h` and `tinyformat.h`.\r\n\r\nTest this PR using:\r\n\r\n```\r\n$ make distclean\r\n$ ./autogen.sh\r\n$ CC=clang CXX=clang++ ./configure --enable-fuzz \\\r\n      --with-sanitizers=address,fuzzer,undefined\r\n$ make\r\n$ src/test/fuzz/locale\r\n…\r\n```\r\n\r\nThe tested functions (`ParseInt32(…)`, `ParseInt64(…)`, `atoi(const std::string&)`, `atoi64(const std::string& str)`, `i64tostr(const char*)`, `itostr(…)`, `strprintf(…)`) all call locale dependent functions (such as `strtol(…)`, `strtoll(…)`, `atoi(const char*)`, etc.) but are assumed to do so in a way that the tested functions return same results regardless of the chosen C locale (`setlocale`).\r\n\r\nThis fuzzer aims to test that those assumptions hold up also in practice now and over time.",
  "closed_by": {
    "login": "maflcko",
    "id": 6399679,
    "node_id": "MDQ6VXNlcjYzOTk2Nzk=",
    "avatar_url": "https://avatars.githubusercontent.com/u/6399679?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/maflcko",
    "html_url": "https://github.com/maflcko",
    "followers_url": "https://api.github.com/users/maflcko/followers",
    "following_url": "https://api.github.com/users/maflcko/following{/other_user}",
    "gists_url": "https://api.github.com/users/maflcko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/maflcko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/maflcko/subscriptions",
    "organizations_url": "https://api.github.com/users/maflcko/orgs",
    "repos_url": "https://api.github.com/users/maflcko/repos",
    "events_url": "https://api.github.com/users/maflcko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/maflcko/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/18126/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
