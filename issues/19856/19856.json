{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856/events",
  "html_url": "https://github.com/bitcoin/bitcoin/issues/19856",
  "id": 690166383,
  "node_id": "MDU6SXNzdWU2OTAxNjYzODM=",
  "number": 19856,
  "title": "Some transactions are not shown in listtransactions output",
  "user": {
    "login": "du2zy",
    "id": 46239138,
    "node_id": "MDQ6VXNlcjQ2MjM5MTM4",
    "avatar_url": "https://avatars.githubusercontent.com/u/46239138?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/du2zy",
    "html_url": "https://github.com/du2zy",
    "followers_url": "https://api.github.com/users/du2zy/followers",
    "following_url": "https://api.github.com/users/du2zy/following{/other_user}",
    "gists_url": "https://api.github.com/users/du2zy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/du2zy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/du2zy/subscriptions",
    "organizations_url": "https://api.github.com/users/du2zy/orgs",
    "repos_url": "https://api.github.com/users/du2zy/repos",
    "events_url": "https://api.github.com/users/du2zy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/du2zy/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 64585,
      "node_id": "MDU6TGFiZWw2NDU4NQ==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Bug",
      "name": "Bug",
      "color": "FBBAAB",
      "default": false,
      "description": null
    },
    {
      "id": 149424,
      "node_id": "MDU6TGFiZWwxNDk0MjQ=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Wallet",
      "name": "Wallet",
      "color": "08a781",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 5,
  "created_at": "2020-09-01T13:57:24Z",
  "updated_at": "2022-12-02T10:00:45Z",
  "closed_at": "2021-12-02T18:38:02Z",
  "author_association": "NONE",
  "active_lock_reason": null,
  "body_html": "<p dir=\"auto\">I have two bitcoin core full nodes. I have one wallet.dat file on both nodes. One node is a master and second node is a reserve. Master signs and sends transactions. Reserve node only receives updates from network. I create new addresses on the master node only and the reserve node derives the same addresses.</p>\n<p dir=\"auto\">I don’t use any other commands on the reserve node. The reserve node creates addresses automatically after receiving a transaction to a new address. This process is possible due to using BIP32 key derivation from one master key.</p>\n<p dir=\"auto\">So, I have constantly synchronized master and reserve nodes. If there are problems with the master, then I switch to the reserve.</p>\n<p dir=\"auto\">I see some strange behavior appear during this process. Some transactions are not shown in the <code class=\"notranslate\">listtransactions</code> output. I did a research an found that only \"in-wallet\" transactions are not shown in the <code class=\"notranslate\">listransactions</code>. The \"in-wallet\" transactions are transactions that were sent with sender’s and receiver’s keys, which are stored in my wallet. I also found that new addresses on the reserve node are created whithout a label.</p>\n<p dir=\"auto\">After adding a label to the address the problem is gone. But adding a label requires additional steps I want to avoid.</p>\n<p dir=\"auto\"><strong>Expected Behavior</strong></p>\n<p dir=\"auto\">Results of <code class=\"notranslate\">listtransactions</code> also shown \"in-wallet\" transactions to addresses without labels</p>\n<p dir=\"auto\"><strong>Current Behavior</strong></p>\n<p dir=\"auto\">Results of <code class=\"notranslate\">listtransactions</code> doesn’t shown \"in-wallet\" transactions to addresses without labels</p>\n<p dir=\"auto\"><strong>Possible Solution</strong></p>\n<ul dir=\"auto\">\n<li>\n<p dir=\"auto\">Automatic adding of a default label to the addresses, derived to the reserve node.</p>\n</li>\n<li>\n<p dir=\"auto\">Print transactions sent to addresses without labels in <code class=\"notranslate\">listtransactions</code>.</p>\n</li>\n</ul>\n<p dir=\"auto\"><strong>Steps to Reproduce</strong></p>\n<ol dir=\"auto\">\n<li>\n<p dir=\"auto\">Run and sync full nodes A and B.</p>\n</li>\n<li>\n<p dir=\"auto\">Stop nodes A and B.</p>\n</li>\n<li>\n<p dir=\"auto\">Copy wallet.dat from node A to node B.</p>\n</li>\n<li>\n<p dir=\"auto\">Run nodes A and B</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">getnewaddress</code> method on node A 10 times. Let’s assume the 10-th address is <code class=\"notranslate\">$ADDRESS</code>.</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">sendtoaddress</code> method on node A. Send 0.001 coins from another address to the <code class=\"notranslate\">$ADDRESS</code>. <code class=\"notranslate\">bitcoin-cli sendtoaddress $ADDRESS 0.001</code>. Let’s assume the result txid is <code class=\"notranslate\">$TXID</code>.</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">listtransaction \"*\"</code> method on node B. You don’t see the <code class=\"notranslate\">$TXID</code> transaction in list.</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">gettransactions</code>, or <code class=\"notranslate\">listunspent</code> methods on node B. You can see the transaction details. Wallet balance changed on node B and is equal to node A.</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">getaddressinfo $ADDRESS</code> method on node A. You can see that address had the default label (<code class=\"notranslate\">\"\"</code>).</p>\n</li>\n<li>\n<p dir=\"auto\">Use <code class=\"notranslate\">getaddressinfo $ADDRESS</code> method on node B. You can see that the address doesn’t have any label.</p>\n</li>\n</ol>\n<p dir=\"auto\"><strong>Context (Environment)</strong></p>\n<p dir=\"auto\">The problem is occured on any version of bitcoin core testnet and mainnet. The problem is also occured on litecoin and any bitcoin-based coins.</p>",
  "body_text": "I have two bitcoin core full nodes. I have one wallet.dat file on both nodes. One node is a master and second node is a reserve. Master signs and sends transactions. Reserve node only receives updates from network. I create new addresses on the master node only and the reserve node derives the same addresses.\nI don’t use any other commands on the reserve node. The reserve node creates addresses automatically after receiving a transaction to a new address. This process is possible due to using BIP32 key derivation from one master key.\nSo, I have constantly synchronized master and reserve nodes. If there are problems with the master, then I switch to the reserve.\nI see some strange behavior appear during this process. Some transactions are not shown in the listtransactions output. I did a research an found that only \"in-wallet\" transactions are not shown in the listransactions. The \"in-wallet\" transactions are transactions that were sent with sender’s and receiver’s keys, which are stored in my wallet. I also found that new addresses on the reserve node are created whithout a label.\nAfter adding a label to the address the problem is gone. But adding a label requires additional steps I want to avoid.\nExpected Behavior\nResults of listtransactions also shown \"in-wallet\" transactions to addresses without labels\nCurrent Behavior\nResults of listtransactions doesn’t shown \"in-wallet\" transactions to addresses without labels\nPossible Solution\n\n\nAutomatic adding of a default label to the addresses, derived to the reserve node.\n\n\nPrint transactions sent to addresses without labels in listtransactions.\n\n\nSteps to Reproduce\n\n\nRun and sync full nodes A and B.\n\n\nStop nodes A and B.\n\n\nCopy wallet.dat from node A to node B.\n\n\nRun nodes A and B\n\n\nUse getnewaddress method on node A 10 times. Let’s assume the 10-th address is $ADDRESS.\n\n\nUse sendtoaddress method on node A. Send 0.001 coins from another address to the $ADDRESS. bitcoin-cli sendtoaddress $ADDRESS 0.001. Let’s assume the result txid is $TXID.\n\n\nUse listtransaction \"*\" method on node B. You don’t see the $TXID transaction in list.\n\n\nUse gettransactions, or listunspent methods on node B. You can see the transaction details. Wallet balance changed on node B and is equal to node A.\n\n\nUse getaddressinfo $ADDRESS method on node A. You can see that address had the default label (\"\").\n\n\nUse getaddressinfo $ADDRESS method on node B. You can see that the address doesn’t have any label.\n\n\nContext (Environment)\nThe problem is occured on any version of bitcoin core testnet and mainnet. The problem is also occured on litecoin and any bitcoin-based coins.",
  "body": "\r\nI have two bitcoin core full nodes. I have one wallet.dat file on both nodes. One node is a master and second node is a reserve. Master signs and sends transactions. Reserve node only receives updates from network. I create new addresses on the master node only and the reserve node derives the same addresses.\r\n\r\nI don’t use any other commands on the reserve node. The reserve node creates addresses automatically after receiving a transaction to a new address. This process is possible due to using BIP32 key derivation from one master key.\r\n\r\nSo, I have constantly synchronized master and reserve nodes. If there are problems with the master, then I switch to the reserve.\r\n\r\nI see some strange behavior appear during this process. Some transactions are not shown in the `listtransactions` output. I did a research an found that only \"in-wallet\" transactions are not shown in the `listransactions`. The \"in-wallet\" transactions are transactions that were sent with sender’s and receiver’s keys, which are stored in my wallet. I also found that new addresses on the reserve node are created whithout a label.\r\n\r\nAfter adding a label to the address the problem is gone. But adding a label requires additional steps I want to avoid.\r\n\r\n**Expected Behavior**\r\n\r\nResults of `listtransactions` also shown \"in-wallet\" transactions to addresses without labels\r\n\r\n\r\n**Current Behavior**\r\n\r\nResults of `listtransactions` doesn’t shown \"in-wallet\" transactions to addresses without labels\r\n\r\n**Possible Solution**\r\n\r\n- Automatic adding of a default label to the addresses, derived to the reserve node.\r\n\r\n- Print transactions sent to addresses without labels in `listtransactions`.\r\n\r\n**Steps to Reproduce**\r\n\r\n1.  Run and sync full nodes A and B.\r\n\r\n2.  Stop nodes A and B.\r\n\r\n3.  Copy wallet.dat from node A to node B.\r\n\r\n4.  Run nodes A and B\r\n\r\n5.  Use `getnewaddress` method on node A 10 times. Let’s assume the 10-th address is `$ADDRESS`.\r\n\r\n6.  Use `sendtoaddress` method on node A. Send 0.001 coins from another address to the `$ADDRESS`. `bitcoin-cli sendtoaddress $ADDRESS 0.001`. Let’s assume the result txid is `$TXID`.\r\n\r\n7.  Use `listtransaction \"*\"` method on node B. You don’t see the `$TXID` transaction in list.\r\n\r\n8.  Use `gettransactions`, or `listunspent` methods on node B. You can see the transaction details. Wallet balance changed on node B and is equal to node A.\r\n\r\n9.  Use `getaddressinfo $ADDRESS` method on node A. You can see that address had the default label (`\"\"`).\r\n\r\n10. Use `getaddressinfo $ADDRESS` method on node B. You can see that the address doesn’t have any label.\r\n\r\n**Context (Environment)**\r\n\r\nThe problem is occured on any version of bitcoin core testnet and mainnet. The problem is also occured on litecoin and any bitcoin-based coins.\r\n\r\n",
  "closed_by": {
    "login": "laanwj",
    "id": 126646,
    "node_id": "MDQ6VXNlcjEyNjY0Ng==",
    "avatar_url": "https://avatars.githubusercontent.com/u/126646?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/laanwj",
    "html_url": "https://github.com/laanwj",
    "followers_url": "https://api.github.com/users/laanwj/followers",
    "following_url": "https://api.github.com/users/laanwj/following{/other_user}",
    "gists_url": "https://api.github.com/users/laanwj/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/laanwj/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/laanwj/subscriptions",
    "organizations_url": "https://api.github.com/users/laanwj/orgs",
    "repos_url": "https://api.github.com/users/laanwj/repos",
    "events_url": "https://api.github.com/users/laanwj/events{/privacy}",
    "received_events_url": "https://api.github.com/users/laanwj/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856/reactions",
    "total_count": 1,
    "+1": 0,
    "-1": 0,
    "laugh": 1,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19856/timeline",
  "performed_via_github_app": null,
  "state_reason": "completed"
}
