{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175/events",
  "html_url": "https://github.com/bitcoin/bitcoin/issues/19175",
  "id": 631535294,
  "node_id": "MDU6SXNzdWU2MzE1MzUyOTQ=",
  "number": 19175,
  "title": "wallet: PeriodicFlush called when mapFileUseCount is empty",
  "user": {
    "login": "fanquake",
    "id": 863730,
    "node_id": "MDQ6VXNlcjg2MzczMA==",
    "avatar_url": "https://avatars.githubusercontent.com/u/863730?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/fanquake",
    "html_url": "https://github.com/fanquake",
    "followers_url": "https://api.github.com/users/fanquake/followers",
    "following_url": "https://api.github.com/users/fanquake/following{/other_user}",
    "gists_url": "https://api.github.com/users/fanquake/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/fanquake/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/fanquake/subscriptions",
    "organizations_url": "https://api.github.com/users/fanquake/orgs",
    "repos_url": "https://api.github.com/users/fanquake/repos",
    "events_url": "https://api.github.com/users/fanquake/events{/privacy}",
    "received_events_url": "https://api.github.com/users/fanquake/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 149424,
      "node_id": "MDU6TGFiZWwxNDk0MjQ=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Wallet",
      "name": "Wallet",
      "color": "08a781",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": false,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 1,
  "created_at": "2020-06-05T11:56:14Z",
  "updated_at": "2023-09-21T16:30:43Z",
  "closed_at": "2023-09-21T16:30:43Z",
  "author_association": "MEMBER",
  "active_lock_reason": null,
  "body_html": "<p dir=\"auto\">I noticed while reviewing <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"626175605\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19085\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19085/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19085\">#19085</a>, if the wallet is due to perform a <a href=\"https://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/db.cpp#L650\"><code class=\"notranslate\">PeriodicFlush()</code></a>, but in the <a href=\"https://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/walletdb.cpp#L966\">2 second delay</a>, another action occurs, i.e a call to <code class=\"notranslate\">backupwallet</code>, the flushing never happens.</p>\n<p dir=\"auto\">The wallet does get <a href=\"https://github.com/bitcoin/bitcoin/blob/master/src/wallet/db.cpp#L708\">flushed and db closed</a> as part of <code class=\"notranslate\">backupwallet</code> (remains loaded); however this leaves the scheduler running <code class=\"notranslate\">MaybeCompactWalletDB</code> continually trying to flush the wallet, which now always fails because <code class=\"notranslate\">mapFileUseCount</code> is empty.</p>\n<p dir=\"auto\">Note that generally, when <code class=\"notranslate\">MaybeCompactWalletDB</code>  is called, nothing is done because:</p>\n<p dir=\"auto\"></p><div class=\"Box Box--condensed my-2\">\n  <div class=\"Box-header f6\">\n    <p class=\"mb-0 text-bold\">\n      <a href=\"https://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/walletdb.cpp#L966\">bitcoin/src/wallet/walletdb.cpp</a>\n    </p>\n    <p class=\"mb-0 color-fg-muted\">\n         Line 966\n      in\n      <a data-pjax=\"true\" class=\"commit-tease-sha Link--inTextBlock\" href=\"/bitcoin/bitcoin/commit/aa35ea55021dbb7f35a00fd666903d9fd03b88e7\">aa35ea5</a>\n    </p>\n  </div>\n  <div itemprop=\"text\" class=\"Box-body p-0 blob-wrapper blob-wrapper-embedded data\">\n    <table class=\"highlight tab-size mb-0 js-file-line-container\" data-tab-size=\"8\" data-paste-markdown-skip=\"\">\n\n        <tbody><tr class=\"border-0\">\n          <td id=\"L966\" class=\"blob-num border-0 px-3 py-0 color-bg-default\" data-line-number=\"966\"></td>\n          <td id=\"LC966\" class=\"blob-code border-0 px-3 py-0 color-bg-default blob-code-inner js-file-line\"> <span class=\"pl-k\">if</span> (dbh.<span class=\"pl-smi\">nLastFlushed</span> != nUpdateCounter &amp;&amp; <span class=\"pl-c1\">GetTime</span>() - dbh.<span class=\"pl-smi\">nLastWalletUpdate</span> &gt;= <span class=\"pl-c1\">2</span>) { </td>\n        </tr>\n    </tbody></table>\n  </div>\n</div>\n<p></p>\n<p dir=\"auto\">is false. However in this case, it's true, and we do continually descend into <code class=\"notranslate\">PeriodicFlush()</code>.</p>\n<p dir=\"auto\">This is resolved whenever the next wallet related action occurs, i.e a call to <code class=\"notranslate\">getwalletinfo</code>, as <code class=\"notranslate\">mapFileUseCount</code> is repopulated, and <code class=\"notranslate\">PeriodicFlush()</code> succeeds.</p>\n<p dir=\"auto\">I don't know the wallet code well enough to know if this is a problem, or just an implementation quirk.</p>",
  "body_text": "I noticed while reviewing #19085, if the wallet is due to perform a PeriodicFlush(), but in the 2 second delay, another action occurs, i.e a call to backupwallet, the flushing never happens.\nThe wallet does get flushed and db closed as part of backupwallet (remains loaded); however this leaves the scheduler running MaybeCompactWalletDB continually trying to flush the wallet, which now always fails because mapFileUseCount is empty.\nNote that generally, when MaybeCompactWalletDB  is called, nothing is done because:\n\n  \n    \n      bitcoin/src/wallet/walletdb.cpp\n    \n    \n         Line 966\n      in\n      aa35ea5\n    \n  \n  \n    \n\n        \n          \n           if (dbh.nLastFlushed != nUpdateCounter && GetTime() - dbh.nLastWalletUpdate >= 2) { \n        \n    \n  \n\n\nis false. However in this case, it's true, and we do continually descend into PeriodicFlush().\nThis is resolved whenever the next wallet related action occurs, i.e a call to getwalletinfo, as mapFileUseCount is repopulated, and PeriodicFlush() succeeds.\nI don't know the wallet code well enough to know if this is a problem, or just an implementation quirk.",
  "body": "I noticed while reviewing #19085, if the wallet is due to perform a [`PeriodicFlush()`](https://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/db.cpp#L650), but in the [2 second delay](https://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/walletdb.cpp#L966), another action occurs, i.e a call to `backupwallet`, the flushing never happens.\r\n\r\nThe wallet does get [flushed and db closed](https://github.com/bitcoin/bitcoin/blob/master/src/wallet/db.cpp#L708) as part of `backupwallet` (remains loaded); however this leaves the scheduler running `MaybeCompactWalletDB` continually trying to flush the wallet, which now always fails because `mapFileUseCount` is empty.\r\n\r\nNote that generally, when `MaybeCompactWalletDB`  is called, nothing is done because:\r\n\r\nhttps://github.com/bitcoin/bitcoin/blob/aa35ea55021dbb7f35a00fd666903d9fd03b88e7/src/wallet/walletdb.cpp#L966\r\n\r\nis false. However in this case, it's true, and we do continually descend into `PeriodicFlush()`.  \r\n\r\nThis is resolved whenever the next wallet related action occurs, i.e a call to `getwalletinfo`, as `mapFileUseCount` is repopulated, and `PeriodicFlush()` succeeds. \r\n\r\nI don't know the wallet code well enough to know if this is a problem, or just an implementation quirk.",
  "closed_by": {
    "login": "willcl-ark",
    "id": 6606587,
    "node_id": "MDQ6VXNlcjY2MDY1ODc=",
    "avatar_url": "https://avatars.githubusercontent.com/u/6606587?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/willcl-ark",
    "html_url": "https://github.com/willcl-ark",
    "followers_url": "https://api.github.com/users/willcl-ark/followers",
    "following_url": "https://api.github.com/users/willcl-ark/following{/other_user}",
    "gists_url": "https://api.github.com/users/willcl-ark/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/willcl-ark/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/willcl-ark/subscriptions",
    "organizations_url": "https://api.github.com/users/willcl-ark/orgs",
    "repos_url": "https://api.github.com/users/willcl-ark/repos",
    "events_url": "https://api.github.com/users/willcl-ark/events{/privacy}",
    "received_events_url": "https://api.github.com/users/willcl-ark/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19175/timeline",
  "performed_via_github_app": null,
  "state_reason": "completed"
}
