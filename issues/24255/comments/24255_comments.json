[
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/1475012160",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/24255#issuecomment-1475012160",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24255",
    "id": 1475012160,
    "node_id": "IC_kwDOABII585X6uZA",
    "user": {
      "login": "pinheadmz",
      "id": 2084648,
      "node_id": "MDQ6VXNlcjIwODQ2NDg=",
      "avatar_url": "https://avatars.githubusercontent.com/u/2084648?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/pinheadmz",
      "html_url": "https://github.com/pinheadmz",
      "followers_url": "https://api.github.com/users/pinheadmz/followers",
      "following_url": "https://api.github.com/users/pinheadmz/following{/other_user}",
      "gists_url": "https://api.github.com/users/pinheadmz/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/pinheadmz/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/pinheadmz/subscriptions",
      "organizations_url": "https://api.github.com/users/pinheadmz/orgs",
      "repos_url": "https://api.github.com/users/pinheadmz/repos",
      "events_url": "https://api.github.com/users/pinheadmz/events{/privacy}",
      "received_events_url": "https://api.github.com/users/pinheadmz/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2023-03-18T22:11:52Z",
    "updated_at": "2023-03-18T22:11:52Z",
    "author_association": "MEMBER",
    "body_html": "<p dir=\"auto\">It's not unusual for a transaction to have 0 block-limit sigops.</p>\n<p dir=\"auto\">The tx in your example has 9 inputs which all spend from legacy p2pkh UTXOs. In each of those spends, the <code class=\"notranslate\">OP_CHECKSIG</code> is literally in the output of the previous transaction, and the sigop cost was \"paid for\" by the funding transactions. The inputs only contain public keys and signatures (no sigops). The single output of that tx is a P2WPKH. The signature operation required to spend that output will be \"paid for\" by the <em>spending</em> transaction: <a href=\"https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#user-content-Sigops\">https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#user-content-Sigops</a></p>\n<p dir=\"auto\">I ran <code class=\"notranslate\">getblocktemplate</code> from my mainnet node and it built a block with 83 0-sigop transactions:</p>\n<div class=\"snippet-clipboard-content notranslate position-relative overflow-auto\" data-snippet-clipboard-copy-content=\"$ bitcoin/src/bitcoin-cli getblocktemplate '{&quot;rules&quot;:[&quot;segwit&quot;]}' | grep -e  '&quot;sigops&quot;: 0' | wc -l\n83\"><pre class=\"notranslate\"><code class=\"notranslate\">$ bitcoin/src/bitcoin-cli getblocktemplate '{\"rules\":[\"segwit\"]}' | grep -e  '\"sigops\": 0' | wc -l\n83\n</code></pre></div>\n<p dir=\"auto\">... most of these are P2TR transactions like <a href=\"https://blockstream.info/tx/0b3caca257f483715bccf70274179f78d8043a88b65df07f295dba40fe0a89f0?expand\" rel=\"nofollow\">https://blockstream.info/tx/0b3caca257f483715bccf70274179f78d8043a88b65df07f295dba40fe0a89f0?expand</a> and in BIP342 it is documented that these sigops do not count towards the block limit at all: <a href=\"https://github.com/bitcoin/bips/blob/master/bip-0342.mediawiki#resource-limits\">https://github.com/bitcoin/bips/blob/master/bip-0342.mediawiki#resource-limits</a></p>",
    "body_text": "It's not unusual for a transaction to have 0 block-limit sigops.\nThe tx in your example has 9 inputs which all spend from legacy p2pkh UTXOs. In each of those spends, the OP_CHECKSIG is literally in the output of the previous transaction, and the sigop cost was \"paid for\" by the funding transactions. The inputs only contain public keys and signatures (no sigops). The single output of that tx is a P2WPKH. The signature operation required to spend that output will be \"paid for\" by the spending transaction: https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#user-content-Sigops\nI ran getblocktemplate from my mainnet node and it built a block with 83 0-sigop transactions:\n$ bitcoin/src/bitcoin-cli getblocktemplate '{\"rules\":[\"segwit\"]}' | grep -e  '\"sigops\": 0' | wc -l\n83\n\n... most of these are P2TR transactions like https://blockstream.info/tx/0b3caca257f483715bccf70274179f78d8043a88b65df07f295dba40fe0a89f0?expand and in BIP342 it is documented that these sigops do not count towards the block limit at all: https://github.com/bitcoin/bips/blob/master/bip-0342.mediawiki#resource-limits",
    "body": "It's not unusual for a transaction to have 0 block-limit sigops.\r\n\r\nThe tx in your example has 9 inputs which all spend from legacy p2pkh UTXOs. In each of those spends, the `OP_CHECKSIG` is literally in the output of the previous transaction, and the sigop cost was \"paid for\" by the funding transactions. The inputs only contain public keys and signatures (no sigops). The single output of that tx is a P2WPKH. The signature operation required to spend that output will be \"paid for\" by the *spending* transaction: https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki#user-content-Sigops\r\n\r\n\r\nI ran `getblocktemplate` from my mainnet node and it built a block with 83 0-sigop transactions:\r\n\r\n```\r\n$ bitcoin/src/bitcoin-cli getblocktemplate '{\"rules\":[\"segwit\"]}' | grep -e  '\"sigops\": 0' | wc -l\r\n83\r\n```\r\n\r\n... most of these are P2TR transactions like https://blockstream.info/tx/0b3caca257f483715bccf70274179f78d8043a88b65df07f295dba40fe0a89f0?expand and in BIP342 it is documented that these sigops do not count towards the block limit at all: https://github.com/bitcoin/bips/blob/master/bip-0342.mediawiki#resource-limits",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/1475012160/reactions",
      "total_count": 1,
      "+1": 1,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  }
]
