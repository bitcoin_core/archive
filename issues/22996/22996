{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/22996",
  "id": 998157301,
  "node_id": "PR_kwDOABII584r1cmx",
  "number": 22996,
  "title": "Better GetHeight() for CTxMemPoolEntry",
  "user": {
    "login": "rebroad",
    "id": 1530283,
    "node_id": "MDQ6VXNlcjE1MzAyODM=",
    "avatar_url": "https://avatars.githubusercontent.com/u/1530283?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rebroad",
    "html_url": "https://github.com/rebroad",
    "followers_url": "https://api.github.com/users/rebroad/followers",
    "following_url": "https://api.github.com/users/rebroad/following{/other_user}",
    "gists_url": "https://api.github.com/users/rebroad/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rebroad/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rebroad/subscriptions",
    "organizations_url": "https://api.github.com/users/rebroad/orgs",
    "repos_url": "https://api.github.com/users/rebroad/repos",
    "events_url": "https://api.github.com/users/rebroad/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rebroad/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [

  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 0,
  "created_at": "2021-09-16T12:30:52Z",
  "updated_at": "2021-09-18T06:03:10Z",
  "closed_at": "2021-09-16T12:34:18Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": null,
  "draft": true,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/22996",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/22996",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/22996.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/22996.patch",
    "merged_at": null
  },
  "body_html": "<p dir=\"auto\">This changes how block height of a transaction in the memory pool is obtained, so that instead of being stored in CTxMemPoolEntry, it is derived when asked for.</p>\n<p dir=\"auto\">This has a few advantages:-</p>\n<ol dir=\"auto\">\n<li>It reduces the memory usage of TxMemPoolEntry</li>\n<li>It allows the correct height to be used for transactions loaded from mempool.dat</li>\n<li>It allows the correct height to be used for transactions saved to the memory pool during initial block download.</li>\n<li>It therefore improves fee estimation given that correct values are being used.</li>\n</ol>\n<p dir=\"auto\">Disadvantages:-</p>\n<ol dir=\"auto\">\n<li>It requires access to the active chain by various functions that previously did not require access, and a lock on cs_main while doing this.</li>\n</ol>\n<p dir=\"auto\">Draft as still testing, but opening to request comments.</p>",
  "body_text": "This changes how block height of a transaction in the memory pool is obtained, so that instead of being stored in CTxMemPoolEntry, it is derived when asked for.\nThis has a few advantages:-\n\nIt reduces the memory usage of TxMemPoolEntry\nIt allows the correct height to be used for transactions loaded from mempool.dat\nIt allows the correct height to be used for transactions saved to the memory pool during initial block download.\nIt therefore improves fee estimation given that correct values are being used.\n\nDisadvantages:-\n\nIt requires access to the active chain by various functions that previously did not require access, and a lock on cs_main while doing this.\n\nDraft as still testing, but opening to request comments.",
  "body": "This changes how block height of a transaction in the memory pool is obtained, so that instead of being stored in CTxMemPoolEntry, it is derived when asked for.\r\n\r\nThis has a few advantages:-\r\n\r\n1. It reduces the memory usage of TxMemPoolEntry\r\n2. It allows the correct height to be used for transactions loaded from mempool.dat\r\n3. It allows the correct height to be used for transactions saved to the memory pool during initial block download.\r\n4. It therefore improves fee estimation given that correct values are being used.\r\n\r\nDisadvantages:-\r\n1. It requires access to the active chain by various functions that previously did not require access, and a lock on cs_main while doing this.\r\n\r\nDraft as still testing, but opening to request comments.",
  "closed_by": {
    "login": "rebroad",
    "id": 1530283,
    "node_id": "MDQ6VXNlcjE1MzAyODM=",
    "avatar_url": "https://avatars.githubusercontent.com/u/1530283?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rebroad",
    "html_url": "https://github.com/rebroad",
    "followers_url": "https://api.github.com/users/rebroad/followers",
    "following_url": "https://api.github.com/users/rebroad/following{/other_user}",
    "gists_url": "https://api.github.com/users/rebroad/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rebroad/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rebroad/subscriptions",
    "organizations_url": "https://api.github.com/users/rebroad/orgs",
    "repos_url": "https://api.github.com/users/rebroad/repos",
    "events_url": "https://api.github.com/users/rebroad/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rebroad/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22996/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
