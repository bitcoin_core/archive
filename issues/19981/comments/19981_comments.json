[
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/696680877",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/19981#issuecomment-696680877",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19981",
    "id": 696680877,
    "node_id": "MDEyOklzc3VlQ29tbWVudDY5NjY4MDg3Nw==",
    "user": {
      "login": "ryanofsky",
      "id": 7133040,
      "node_id": "MDQ6VXNlcjcxMzMwNDA=",
      "avatar_url": "https://avatars.githubusercontent.com/u/7133040?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/ryanofsky",
      "html_url": "https://github.com/ryanofsky",
      "followers_url": "https://api.github.com/users/ryanofsky/followers",
      "following_url": "https://api.github.com/users/ryanofsky/following{/other_user}",
      "gists_url": "https://api.github.com/users/ryanofsky/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/ryanofsky/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/ryanofsky/subscriptions",
      "organizations_url": "https://api.github.com/users/ryanofsky/orgs",
      "repos_url": "https://api.github.com/users/ryanofsky/repos",
      "events_url": "https://api.github.com/users/ryanofsky/events{/privacy}",
      "received_events_url": "https://api.github.com/users/ryanofsky/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2020-09-22T12:11:56Z",
    "updated_at": "2020-09-22T12:11:56Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">Here is what I've been able to figure out with this issue so far.</p>\n<ol dir=\"auto\">\n<li>\n<p dir=\"auto\">I don't think there's a travis caching problem. Travis passing on master but failing in the 726983595 build is just a result of travis job 726983595 above executing <code class=\"notranslate\">mpgen</code> in the bitcoin build and current master not doing that. Any changes to <code class=\"notranslate\">funcs.mk</code> changes are also reflected in build ids so if build ids are used correctly, there probably shouldn't be a caching issue.</p>\n</li>\n<li>\n<p dir=\"auto\">Confirmed setting LDFLAGS variable is the thing that causes RUNPATH not to be set. LDFLAGS environment value gets copied into CMAKE_EXE_LINKER_FLAGS cached global value, which otherwise would be empty. So prior to <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"675386095\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19685\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19685/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19685\">#19685</a>, LDFLAGS/CMAKE_EXE_LINKER_FLAGS value was empty. After <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"675386095\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19685\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19685/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19685\">#19685</a> it is \"-L/home/russ/src/bitcoin/depends/x86_64-pc-linux-gnu/native/lib\"</p>\n</li>\n<li>\n<p dir=\"auto\">So far I don't see a clean fix for this. Partially reverting <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"675386095\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19685\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19685/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19685\">#19685</a> and not passing LDFLAGS for native packages is probably not ideal. Overriding LDFLAGS for native_libmultiprocess in particular is hacky. I haven't figured out exactly how setting LDFLAGS does prevent mpgen RUNPATH from being set. MPGEN target has <a href=\"https://github.com/chaincodelabs/libmultiprocess/blob/4c599773923afe6829139ce682d723d769e335dc/CMakeLists.txt#L82\"><code class=\"notranslate\">INSTALL_RPATH_USE_LINK_PATH</code></a> set to true. If LDFLAGS is empty or set to harmless value like <code class=\"notranslate\">-lm</code> with no <code class=\"notranslate\">-L</code> link path, RUNPATH is set. But as soon as <code class=\"notranslate\">-L</code> is added, RUNPATH disappears. I think this might have to do with logic <a href=\"https://gitlab.kitware.com/cmake/cmake/-/blob/11425041f04fd0945480b8f9e9933d1549b93981/Source/cmComputeLinkInformation.cxx#L1720\" rel=\"nofollow\">https://gitlab.kitware.com/cmake/cmake/-/blob/11425041f04fd0945480b8f9e9933d1549b93981/Source/cmComputeLinkInformation.cxx#L1720</a>, but so far I don't understand it. I might experiment with LIBRARY_PATH environment variable, but even if this works it would need to be set for the native packages, unset for native packages, so would add unappealing complication.</p>\n</li>\n<li>\n<p dir=\"auto\">Fastest way to test this is to hardcode capnp package build id so it doesn't get rebuilt with every change to <code class=\"notranslate\">funcs.mk</code></p>\n</li>\n</ol>\n<div class=\"highlight highlight-source-diff notranslate position-relative overflow-auto\" dir=\"auto\" data-snippet-clipboard-copy-content=\"--- a/depends/funcs.mk\n+++ b/depends/funcs.mk\n@@ -45,6 +45,7 @@ $(eval $(1)_all_dependencies:=$(call int_get_all_dependencies,$(1),$($($(1)_type\n $(foreach dep,$($(1)_all_dependencies),$(eval $(1)_build_id_deps+=$(dep)-$($(dep)_version)-$($(dep)_recipe_hash)))\n $(eval $(1)_build_id_long:=$(1)-$($(1)_version)-$($(1)_recipe_hash)-$(release_type) $($(1)_build_id_deps) $($($(1)_type)_id_string))\n $(eval $(1)_build_id:=$(shell echo -n &quot;$($(1)_build_id_long)&quot; | $(build_SHA256SUM) | cut -c-$(HASH_LENGTH)))\n+$(eval native_capnp_build_id:=f5a3acc1fc3)\n final_build_id_long+=$($(package)_build_id_long)\n \n #compute package-specific paths\"><pre class=\"notranslate\"><span class=\"pl-md\">--- a/depends/funcs.mk</span>\n<span class=\"pl-mi1\">+++ b/depends/funcs.mk</span>\n<span class=\"pl-mdr\">@@ -45,6 +45,7 @@</span> $(eval $(1)_all_dependencies:=$(call int_get_all_dependencies,$(1),$($($(1)_type\n $(foreach dep,$($(1)_all_dependencies),$(eval $(1)_build_id_deps+=$(dep)-$($(dep)_version)-$($(dep)_recipe_hash)))\n $(eval $(1)_build_id_long:=$(1)-$($(1)_version)-$($(1)_recipe_hash)-$(release_type) $($(1)_build_id_deps) $($($(1)_type)_id_string))\n $(eval $(1)_build_id:=$(shell echo -n \"$($(1)_build_id_long)\" | $(build_SHA256SUM) | cut -c-$(HASH_LENGTH)))\n<span class=\"pl-mi1\"><span class=\"pl-mi1\">+</span>$(eval native_capnp_build_id:=f5a3acc1fc3)</span>\n final_build_id_long+=$($(package)_build_id_long)\n \n #compute package-specific paths</pre></div>\n<p dir=\"auto\">After that resulting RUNPATH can be checked with:</p>\n<div class=\"highlight highlight-source-shell notranslate position-relative overflow-auto\" dir=\"auto\" data-snippet-clipboard-copy-content=\"rm -rvf depends/x86_64-pc-linux-gnu/native depends/work/staging depends/work/build\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_staged\nfor f in `find -name mpgen`; do echo == $f ==; readelf -d $f | grep -i path; done\"><pre class=\"notranslate\">rm -rvf depends/x86_64-pc-linux-gnu/native depends/work/staging depends/work/build\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_staged\n<span class=\"pl-k\">for</span> <span class=\"pl-smi\">f</span> <span class=\"pl-k\">in</span> <span class=\"pl-s\"><span class=\"pl-pds\">`</span>find -name mpgen<span class=\"pl-pds\">`</span></span><span class=\"pl-k\">;</span> <span class=\"pl-k\">do</span> <span class=\"pl-c1\">echo</span> == <span class=\"pl-smi\">$f</span> ==<span class=\"pl-k\">;</span> readelf -d <span class=\"pl-smi\">$f</span> <span class=\"pl-k\">|</span> grep -i path<span class=\"pl-k\">;</span> <span class=\"pl-k\">done</span></pre></div>\n<p dir=\"auto\">CMakeCache can be examined with:</p>\n<div class=\"highlight highlight-source-shell notranslate position-relative overflow-auto\" dir=\"auto\" data-snippet-clipboard-copy-content=\"make -C depends MULTIPROCESS=1 native_libmultiprocess_built\nfind -name CMakeCache.txt\"><pre class=\"notranslate\">make -C depends MULTIPROCESS=1 native_libmultiprocess_built\nfind -name CMakeCache.txt</pre></div>",
    "body_text": "Here is what I've been able to figure out with this issue so far.\n\n\nI don't think there's a travis caching problem. Travis passing on master but failing in the 726983595 build is just a result of travis job 726983595 above executing mpgen in the bitcoin build and current master not doing that. Any changes to funcs.mk changes are also reflected in build ids so if build ids are used correctly, there probably shouldn't be a caching issue.\n\n\nConfirmed setting LDFLAGS variable is the thing that causes RUNPATH not to be set. LDFLAGS environment value gets copied into CMAKE_EXE_LINKER_FLAGS cached global value, which otherwise would be empty. So prior to #19685, LDFLAGS/CMAKE_EXE_LINKER_FLAGS value was empty. After #19685 it is \"-L/home/russ/src/bitcoin/depends/x86_64-pc-linux-gnu/native/lib\"\n\n\nSo far I don't see a clean fix for this. Partially reverting #19685 and not passing LDFLAGS for native packages is probably not ideal. Overriding LDFLAGS for native_libmultiprocess in particular is hacky. I haven't figured out exactly how setting LDFLAGS does prevent mpgen RUNPATH from being set. MPGEN target has INSTALL_RPATH_USE_LINK_PATH set to true. If LDFLAGS is empty or set to harmless value like -lm with no -L link path, RUNPATH is set. But as soon as -L is added, RUNPATH disappears. I think this might have to do with logic https://gitlab.kitware.com/cmake/cmake/-/blob/11425041f04fd0945480b8f9e9933d1549b93981/Source/cmComputeLinkInformation.cxx#L1720, but so far I don't understand it. I might experiment with LIBRARY_PATH environment variable, but even if this works it would need to be set for the native packages, unset for native packages, so would add unappealing complication.\n\n\nFastest way to test this is to hardcode capnp package build id so it doesn't get rebuilt with every change to funcs.mk\n\n\n--- a/depends/funcs.mk\n+++ b/depends/funcs.mk\n@@ -45,6 +45,7 @@ $(eval $(1)_all_dependencies:=$(call int_get_all_dependencies,$(1),$($($(1)_type\n $(foreach dep,$($(1)_all_dependencies),$(eval $(1)_build_id_deps+=$(dep)-$($(dep)_version)-$($(dep)_recipe_hash)))\n $(eval $(1)_build_id_long:=$(1)-$($(1)_version)-$($(1)_recipe_hash)-$(release_type) $($(1)_build_id_deps) $($($(1)_type)_id_string))\n $(eval $(1)_build_id:=$(shell echo -n \"$($(1)_build_id_long)\" | $(build_SHA256SUM) | cut -c-$(HASH_LENGTH)))\n+$(eval native_capnp_build_id:=f5a3acc1fc3)\n final_build_id_long+=$($(package)_build_id_long)\n \n #compute package-specific paths\nAfter that resulting RUNPATH can be checked with:\nrm -rvf depends/x86_64-pc-linux-gnu/native depends/work/staging depends/work/build\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_staged\nfor f in `find -name mpgen`; do echo == $f ==; readelf -d $f | grep -i path; done\nCMakeCache can be examined with:\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_built\nfind -name CMakeCache.txt",
    "body": "Here is what I've been able to figure out with this issue so far.\r\n\r\n1. I don't think there's a travis caching problem. Travis passing on master but failing in the 726983595 build is just a result of travis job 726983595 above executing `mpgen` in the bitcoin build and current master not doing that. Any changes to `funcs.mk` changes are also reflected in build ids so if build ids are used correctly, there probably shouldn't be a caching issue.\r\n\r\n2. Confirmed setting LDFLAGS variable is the thing that causes RUNPATH not to be set. LDFLAGS environment value gets copied into CMAKE_EXE_LINKER_FLAGS cached global value, which otherwise would be empty. So prior to #19685, LDFLAGS/CMAKE_EXE_LINKER_FLAGS value was empty. After #19685 it is \"-L/home/russ/src/bitcoin/depends/x86_64-pc-linux-gnu/native/lib\"\r\n\r\n3. So far I don't see a clean fix for this. Partially reverting #19685 and not passing LDFLAGS for native packages is probably not ideal. Overriding LDFLAGS for native_libmultiprocess in particular is hacky. I haven't figured out exactly how setting LDFLAGS does prevent mpgen RUNPATH from being set. MPGEN target has [`INSTALL_RPATH_USE_LINK_PATH`](https://github.com/chaincodelabs/libmultiprocess/blob/4c599773923afe6829139ce682d723d769e335dc/CMakeLists.txt#L82) set to true. If LDFLAGS is empty or set to harmless value like `-lm` with no `-L` link path, RUNPATH is set. But as soon as `-L` is added, RUNPATH disappears. I think this might have to do with logic https://gitlab.kitware.com/cmake/cmake/-/blob/11425041f04fd0945480b8f9e9933d1549b93981/Source/cmComputeLinkInformation.cxx#L1720, but so far I don't understand it. I might experiment with LIBRARY_PATH environment variable, but even if this works it would need to be set for the native packages, unset for native packages, so would add unappealing complication. \r\n\r\n4. Fastest way to test this is to hardcode capnp package build id so it doesn't get rebuilt with every change to `funcs.mk`\r\n\r\n```diff\r\n--- a/depends/funcs.mk\r\n+++ b/depends/funcs.mk\r\n@@ -45,6 +45,7 @@ $(eval $(1)_all_dependencies:=$(call int_get_all_dependencies,$(1),$($($(1)_type\r\n $(foreach dep,$($(1)_all_dependencies),$(eval $(1)_build_id_deps+=$(dep)-$($(dep)_version)-$($(dep)_recipe_hash)))\r\n $(eval $(1)_build_id_long:=$(1)-$($(1)_version)-$($(1)_recipe_hash)-$(release_type) $($(1)_build_id_deps) $($($(1)_type)_id_string))\r\n $(eval $(1)_build_id:=$(shell echo -n \"$($(1)_build_id_long)\" | $(build_SHA256SUM) | cut -c-$(HASH_LENGTH)))\r\n+$(eval native_capnp_build_id:=f5a3acc1fc3)\r\n final_build_id_long+=$($(package)_build_id_long)\r\n \r\n #compute package-specific paths\r\n```\r\n\r\nAfter that resulting RUNPATH can be checked with:\r\n\r\n```sh\r\nrm -rvf depends/x86_64-pc-linux-gnu/native depends/work/staging depends/work/build\r\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_staged\r\nfor f in `find -name mpgen`; do echo == $f ==; readelf -d $f | grep -i path; done\r\n```\r\n\r\nCMakeCache can be examined with:\r\n\r\n```sh\r\nmake -C depends MULTIPROCESS=1 native_libmultiprocess_built\r\nfind -name CMakeCache.txt\r\n```",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/696680877/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/696898685",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/19981#issuecomment-696898685",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19981",
    "id": 696898685,
    "node_id": "MDEyOklzc3VlQ29tbWVudDY5Njg5ODY4NQ==",
    "user": {
      "login": "ryanofsky",
      "id": 7133040,
      "node_id": "MDQ6VXNlcjcxMzMwNDA=",
      "avatar_url": "https://avatars.githubusercontent.com/u/7133040?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/ryanofsky",
      "html_url": "https://github.com/ryanofsky",
      "followers_url": "https://api.github.com/users/ryanofsky/followers",
      "following_url": "https://api.github.com/users/ryanofsky/following{/other_user}",
      "gists_url": "https://api.github.com/users/ryanofsky/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/ryanofsky/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/ryanofsky/subscriptions",
      "organizations_url": "https://api.github.com/users/ryanofsky/orgs",
      "repos_url": "https://api.github.com/users/ryanofsky/repos",
      "events_url": "https://api.github.com/users/ryanofsky/events{/privacy}",
      "received_events_url": "https://api.github.com/users/ryanofsky/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2020-09-22T18:24:38Z",
    "updated_at": "2020-09-22T18:24:38Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">I think I found a decent workaround setting CMAKE_INSTALL_RPATH. If it works I will incorporate it in <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"630098657\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19160\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19160/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19160\">#19160</a> and close this issue. Setting LIBRARY_PATH environment variable didn't have any effect (was just ignored).</p>\n<p dir=\"auto\">Also posted a question about this upstream <a href=\"https://discourse.cmake.org/t/install-rpath-use-link-path-not-working-when-cmake-exe-linker-flags-ldflags-is-set/1892\" rel=\"nofollow\">https://discourse.cmake.org/t/install-rpath-use-link-path-not-working-when-cmake-exe-linker-flags-ldflags-is-set/1892</a></p>",
    "body_text": "I think I found a decent workaround setting CMAKE_INSTALL_RPATH. If it works I will incorporate it in #19160 and close this issue. Setting LIBRARY_PATH environment variable didn't have any effect (was just ignored).\nAlso posted a question about this upstream https://discourse.cmake.org/t/install-rpath-use-link-path-not-working-when-cmake-exe-linker-flags-ldflags-is-set/1892",
    "body": "I think I found a decent workaround setting CMAKE_INSTALL_RPATH. If it works I will incorporate it in #19160 and close this issue. Setting LIBRARY_PATH environment variable didn't have any effect (was just ignored).\r\n\r\nAlso posted a question about this upstream https://discourse.cmake.org/t/install-rpath-use-link-path-not-working-when-cmake-exe-linker-flags-ldflags-is-set/1892",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/696898685/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  },
  {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/698921837",
    "html_url": "https://github.com/bitcoin/bitcoin/issues/19981#issuecomment-698921837",
    "issue_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/19981",
    "id": 698921837,
    "node_id": "MDEyOklzc3VlQ29tbWVudDY5ODkyMTgzNw==",
    "user": {
      "login": "ryanofsky",
      "id": 7133040,
      "node_id": "MDQ6VXNlcjcxMzMwNDA=",
      "avatar_url": "https://avatars.githubusercontent.com/u/7133040?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/ryanofsky",
      "html_url": "https://github.com/ryanofsky",
      "followers_url": "https://api.github.com/users/ryanofsky/followers",
      "following_url": "https://api.github.com/users/ryanofsky/following{/other_user}",
      "gists_url": "https://api.github.com/users/ryanofsky/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/ryanofsky/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/ryanofsky/subscriptions",
      "organizations_url": "https://api.github.com/users/ryanofsky/orgs",
      "repos_url": "https://api.github.com/users/ryanofsky/repos",
      "events_url": "https://api.github.com/users/ryanofsky/events{/privacy}",
      "received_events_url": "https://api.github.com/users/ryanofsky/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2020-09-25T13:15:43Z",
    "updated_at": "2020-09-25T13:15:43Z",
    "author_association": "CONTRIBUTOR",
    "body_html": "<p dir=\"auto\">Will close this issue. I confirmed <a class=\"commit-link\" data-hovercard-type=\"commit\" data-hovercard-url=\"https://github.com/bitcoin/bitcoin/commit/01a7b773606c7304e5ca42f6ff121d818c972361/hovercard\" href=\"https://github.com/bitcoin/bitcoin/commit/01a7b773606c7304e5ca42f6ff121d818c972361\"><tt>01a7b77</tt></a> from <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"630098657\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19160\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19160/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19160\">#19160</a> fixes the problem, or at least works around it: <a href=\"https://travis-ci.org/github/bitcoin/bitcoin/jobs/730006938\" rel=\"nofollow\">https://travis-ci.org/github/bitcoin/bitcoin/jobs/730006938</a>.</p>\n<p dir=\"auto\">I could make a separate PR for the fix since it's a build change pretty different than the other changes in <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"630098657\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/19160\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/19160/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/19160\">#19160</a>. On the other hand, a reason for keeping it in the same PR is that other changes in the PR are needed to verify the fix works on travis. Maybe it would be a good idea in the future for the depends build to have a test target (particularly for native packages, but checks could be done on cross-compiled packages too), so build fixes like <a class=\"commit-link\" data-hovercard-type=\"commit\" data-hovercard-url=\"https://github.com/bitcoin/bitcoin/commit/01a7b773606c7304e5ca42f6ff121d818c972361/hovercard\" href=\"https://github.com/bitcoin/bitcoin/commit/01a7b773606c7304e5ca42f6ff121d818c972361\"><tt>01a7b77</tt></a> could be more separable and testable.</p>",
    "body_text": "Will close this issue. I confirmed 01a7b77 from #19160 fixes the problem, or at least works around it: https://travis-ci.org/github/bitcoin/bitcoin/jobs/730006938.\nI could make a separate PR for the fix since it's a build change pretty different than the other changes in #19160. On the other hand, a reason for keeping it in the same PR is that other changes in the PR are needed to verify the fix works on travis. Maybe it would be a good idea in the future for the depends build to have a test target (particularly for native packages, but checks could be done on cross-compiled packages too), so build fixes like 01a7b77 could be more separable and testable.",
    "body": "Will close this issue. I confirmed 01a7b773606c7304e5ca42f6ff121d818c972361 from #19160 fixes the problem, or at least works around it: https://travis-ci.org/github/bitcoin/bitcoin/jobs/730006938.\r\n\r\nI could make a separate PR for the fix since it's a build change pretty different than the other changes in #19160. On the other hand, a reason for keeping it in the same PR is that other changes in the PR are needed to verify the fix works on travis. Maybe it would be a good idea in the future for the depends build to have a test target (particularly for native packages, but checks could be done on cross-compiled packages too), so build fixes like 01a7b773606c7304e5ca42f6ff121d818c972361 could be more separable and testable.",
    "reactions": {
      "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/comments/698921837/reactions",
      "total_count": 0,
      "+1": 0,
      "-1": 0,
      "laugh": 0,
      "hooray": 0,
      "confused": 0,
      "heart": 0,
      "rocket": 0,
      "eyes": 0
    },
    "performed_via_github_app": null
  }
]
