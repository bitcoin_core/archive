{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866/events",
  "html_url": "https://github.com/bitcoin/bitcoin/issues/22866",
  "id": 987171900,
  "node_id": "MDU6SXNzdWU5ODcxNzE5MDA=",
  "number": 22866,
  "title": "Add CBOR RPC interface",
  "user": {
    "login": "GeneFerneau",
    "id": 80422284,
    "node_id": "MDQ6VXNlcjgwNDIyMjg0",
    "avatar_url": "https://avatars.githubusercontent.com/u/80422284?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/GeneFerneau",
    "html_url": "https://github.com/GeneFerneau",
    "followers_url": "https://api.github.com/users/GeneFerneau/followers",
    "following_url": "https://api.github.com/users/GeneFerneau/following{/other_user}",
    "gists_url": "https://api.github.com/users/GeneFerneau/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/GeneFerneau/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/GeneFerneau/subscriptions",
    "organizations_url": "https://api.github.com/users/GeneFerneau/orgs",
    "repos_url": "https://api.github.com/users/GeneFerneau/repos",
    "events_url": "https://api.github.com/users/GeneFerneau/events{/privacy}",
    "received_events_url": "https://api.github.com/users/GeneFerneau/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 64583,
      "node_id": "MDU6TGFiZWw2NDU4Mw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Feature",
      "name": "Feature",
      "color": "7cf575",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 6,
  "created_at": "2021-09-02T21:15:48Z",
  "updated_at": "2024-03-09T12:37:37Z",
  "closed_at": "2023-03-10T20:27:06Z",
  "author_association": "NONE",
  "active_lock_reason": null,
  "body_html": "<p dir=\"auto\">This issue is a proposal to add a CBOR RPC interface to bitcoin-core. The interface would be in addition to the current JSON-RPC, not a replacement.</p>\n<p dir=\"auto\">The main goal of the proposal is to offer an RPC interface that is more efficient than the current JSON-RPC. Remote clients with constraints on data usage will see the biggest advantage.</p>\n<p dir=\"auto\">CBOR seems like the best choice for a minimal RPC implementation, since it has wide support in industry, and is the de-facto standard for IoT communication protocols like <a href=\"https://en.wikipedia.org/wiki/CoAP\" rel=\"nofollow\">CoAP</a>. Meaning, there is likely to be a large community outside Bitcoin to get support/developers.</p>\n<h2 dir=\"auto\">Advantages</h2>\n<ul dir=\"auto\">\n<li>RFC standard specification: <a href=\"https://datatracker.ietf.org/doc/html/rfc8949\" rel=\"nofollow\">RFC 8949</a></li>\n<li>CBOR uses binary representation\n<ul dir=\"auto\">\n<li>no need for Base64 or other encoding</li>\n<li>well-defined data types with minimal encoding overhead</li>\n</ul>\n</li>\n<li>Small amount of types</li>\n<li>Several existing free open-source libraries to fork</li>\n<li>Many existing implementations are small (~400-750 LoC)</li>\n<li>Decreased data transmission for remote clients</li>\n</ul>\n<h2 dir=\"auto\">Disadvantages</h2>\n<ul dir=\"auto\">\n<li>Added code to the core implementation\n<ul dir=\"auto\">\n<li>larger attack surface</li>\n<li>maintenance costs</li>\n</ul>\n</li>\n<li>Added complexity</li>\n<li>Unclear data-size savings versus compressed JSON/Base64</li>\n</ul>\n<h2 dir=\"auto\">Alternative approaches</h2>\n<h3 dir=\"auto\">External CBOR proxy</h3>\n<p dir=\"auto\">One possible alternative, suggested by <a class=\"user-mention notranslate\" data-hovercard-type=\"user\" data-hovercard-url=\"/users/cfields/hovercard\" data-octo-click=\"hovercard-link-click\" data-octo-dimensions=\"link_type:self\" href=\"https://github.com/cfields\">@cfields</a>, is to write a proxy translating CBOR to JSON.</p>\n<p dir=\"auto\">The proxy could be stand-alone from bitcoin-core, thus removing the disadvantage of added code.</p>\n<p dir=\"auto\">Also, the proxy could be written in a memory-safe language like Rust, decreasing the attack surface.</p>\n<p dir=\"auto\">There would also be disadvantages to a proxy implementation:</p>\n<ul dir=\"auto\">\n<li>small overhead of translating CBOR to JSON, and passing to the original JSON-RPC</li>\n<li>installation of an additional piece of software</li>\n</ul>\n<h3 dir=\"auto\">Internal CBOR proxy</h3>\n<p dir=\"auto\">Another alternative is to add CBOR support to Univalue, and implement the proxy in bitcoin-core.</p>\n<p dir=\"auto\">This would have the advantage of direct access to RPC internals, and potentially reduce overall code size.</p>\n<p dir=\"auto\">The proxy would listen on a separate port, translate the incoming CBOR to JSON, and pass the JSON to existing RPC interfaces.</p>\n<h2 dir=\"auto\">Free open-source implementations</h2>\n<p dir=\"auto\">Here is a list of some of the better candidates for a bitcoin-core CBOR fork:</p>\n<ul dir=\"auto\">\n<li>cb0r: <a href=\"https://github.com/quartzjer/cb0r\">https://github.com/quartzjer/cb0r</a>\n<ul dir=\"auto\">\n<li>zero-allocation C implementation</li>\n</ul>\n</li>\n<li>cppbor: <a href=\"https://github.com/rantydave/cppbor\">https://github.com/rantydave/cppbor</a>\n<ul dir=\"auto\">\n<li>C++17 implementation based on <code class=\"notranslate\">std::variant</code></li>\n</ul>\n</li>\n<li>cbor11: <a href=\"https://github.com/jakobvarmose/cbor11\">https://github.com/jakobvarmose/cbor11</a>\n<ul dir=\"auto\">\n<li>C++11 implementation</li>\n</ul>\n</li>\n<li>tinycbor: <a href=\"https://github.com/intel/tinycbor\">https://github.com/intel/tinycbor</a>\n<ul dir=\"auto\">\n<li>Intel implementation, includes CBOR-JSON translation</li>\n<li>ironically, largest LoC count of the candidates</li>\n</ul>\n</li>\n<li>cbor-lite: <a href=\"https://bitbucket.org/isode/cbor-lite\" rel=\"nofollow\">https://bitbucket.org/isode/cbor-lite</a>\n<ul dir=\"auto\">\n<li>C++14 implementation, header-only</li>\n<li>used in <a href=\"https://github.com/BlockchainCommons/bc-ur/blob/master/src/cbor-lite.hpp\">bc-ur</a></li>\n</ul>\n</li>\n</ul>\n<h2 dir=\"auto\">Comparing to JSON-RPC</h2>\n<p dir=\"auto\"><a class=\"user-mention notranslate\" data-hovercard-type=\"user\" data-hovercard-url=\"/users/laanwj/hovercard\" data-octo-click=\"hovercard-link-click\" data-octo-dimensions=\"link_type:self\" href=\"https://github.com/laanwj\">@laanwj</a> raised the point that compressed JSON may provide similar savings to CBOR. To test whether the savings from CBOR provides significant size reduction, I will implement a small number of RPC calls in CBOR,<br>\nand compare the uncompressed and compressed sizes against the current JSON encoding.</p>",
  "body_text": "This issue is a proposal to add a CBOR RPC interface to bitcoin-core. The interface would be in addition to the current JSON-RPC, not a replacement.\nThe main goal of the proposal is to offer an RPC interface that is more efficient than the current JSON-RPC. Remote clients with constraints on data usage will see the biggest advantage.\nCBOR seems like the best choice for a minimal RPC implementation, since it has wide support in industry, and is the de-facto standard for IoT communication protocols like CoAP. Meaning, there is likely to be a large community outside Bitcoin to get support/developers.\nAdvantages\n\nRFC standard specification: RFC 8949\nCBOR uses binary representation\n\nno need for Base64 or other encoding\nwell-defined data types with minimal encoding overhead\n\n\nSmall amount of types\nSeveral existing free open-source libraries to fork\nMany existing implementations are small (~400-750 LoC)\nDecreased data transmission for remote clients\n\nDisadvantages\n\nAdded code to the core implementation\n\nlarger attack surface\nmaintenance costs\n\n\nAdded complexity\nUnclear data-size savings versus compressed JSON/Base64\n\nAlternative approaches\nExternal CBOR proxy\nOne possible alternative, suggested by @cfields, is to write a proxy translating CBOR to JSON.\nThe proxy could be stand-alone from bitcoin-core, thus removing the disadvantage of added code.\nAlso, the proxy could be written in a memory-safe language like Rust, decreasing the attack surface.\nThere would also be disadvantages to a proxy implementation:\n\nsmall overhead of translating CBOR to JSON, and passing to the original JSON-RPC\ninstallation of an additional piece of software\n\nInternal CBOR proxy\nAnother alternative is to add CBOR support to Univalue, and implement the proxy in bitcoin-core.\nThis would have the advantage of direct access to RPC internals, and potentially reduce overall code size.\nThe proxy would listen on a separate port, translate the incoming CBOR to JSON, and pass the JSON to existing RPC interfaces.\nFree open-source implementations\nHere is a list of some of the better candidates for a bitcoin-core CBOR fork:\n\ncb0r: https://github.com/quartzjer/cb0r\n\nzero-allocation C implementation\n\n\ncppbor: https://github.com/rantydave/cppbor\n\nC++17 implementation based on std::variant\n\n\ncbor11: https://github.com/jakobvarmose/cbor11\n\nC++11 implementation\n\n\ntinycbor: https://github.com/intel/tinycbor\n\nIntel implementation, includes CBOR-JSON translation\nironically, largest LoC count of the candidates\n\n\ncbor-lite: https://bitbucket.org/isode/cbor-lite\n\nC++14 implementation, header-only\nused in bc-ur\n\n\n\nComparing to JSON-RPC\n@laanwj raised the point that compressed JSON may provide similar savings to CBOR. To test whether the savings from CBOR provides significant size reduction, I will implement a small number of RPC calls in CBOR,\nand compare the uncompressed and compressed sizes against the current JSON encoding.",
  "body": "This issue is a proposal to add a CBOR RPC interface to bitcoin-core. The interface would be in addition to the current JSON-RPC, not a replacement.\r\n\r\nThe main goal of the proposal is to offer an RPC interface that is more efficient than the current JSON-RPC. Remote clients with constraints on data usage will see the biggest advantage.\r\n\r\nCBOR seems like the best choice for a minimal RPC implementation, since it has wide support in industry, and is the de-facto standard for IoT communication protocols like [CoAP](https://en.wikipedia.org/wiki/CoAP\r\n). Meaning, there is likely to be a large community outside Bitcoin to get support/developers.\r\n\r\n## Advantages\r\n\r\n- RFC standard specification: [RFC 8949](https://datatracker.ietf.org/doc/html/rfc8949)\r\n- CBOR uses binary representation\r\n  - no need for Base64 or other encoding\r\n  - well-defined data types with minimal encoding overhead\r\n- Small amount of types\r\n- Several existing free open-source libraries to fork\r\n- Many existing implementations are small (~400-750 LoC)\r\n- Decreased data transmission for remote clients\r\n\r\n## Disadvantages\r\n\r\n- Added code to the core implementation\r\n  - larger attack surface\r\n  - maintenance costs\r\n- Added complexity\r\n- Unclear data-size savings versus compressed JSON/Base64\r\n\r\n## Alternative approaches\r\n\r\n### External CBOR proxy\r\n\r\nOne possible alternative, suggested by @cfields, is to write a proxy translating CBOR to JSON.\r\n\r\nThe proxy could be stand-alone from bitcoin-core, thus removing the disadvantage of added code.\r\n\r\nAlso, the proxy could be written in a memory-safe language like Rust, decreasing the attack surface.\r\n\r\nThere would also be disadvantages to a proxy implementation:\r\n\r\n- small overhead of translating CBOR to JSON, and passing to the original JSON-RPC\r\n- installation of an additional piece of software\r\n\r\n### Internal CBOR proxy\r\n\r\nAnother alternative is to add CBOR support to Univalue, and implement the proxy in bitcoin-core.\r\n\r\nThis would have the advantage of direct access to RPC internals, and potentially reduce overall code size.\r\n\r\nThe proxy would listen on a separate port, translate the incoming CBOR to JSON, and pass the JSON to existing RPC interfaces.\r\n\r\n## Free open-source implementations\r\n\r\nHere is a list of some of the better candidates for a bitcoin-core CBOR fork:\r\n\r\n- cb0r: https://github.com/quartzjer/cb0r\r\n  - zero-allocation C implementation\r\n- cppbor: https://github.com/rantydave/cppbor\r\n  - C++17 implementation based on `std::variant`\r\n- cbor11: https://github.com/jakobvarmose/cbor11\r\n  - C++11 implementation\r\n- tinycbor: https://github.com/intel/tinycbor\r\n  - Intel implementation, includes CBOR-JSON translation\r\n  - ironically, largest LoC count of the candidates\r\n- cbor-lite: https://bitbucket.org/isode/cbor-lite\r\n  - C++14 implementation, header-only\r\n  - used in [bc-ur](https://github.com/BlockchainCommons/bc-ur/blob/master/src/cbor-lite.hpp) \r\n\r\n## Comparing to JSON-RPC\r\n\r\n@laanwj raised the point that compressed JSON may provide similar savings to CBOR. To test whether the savings from CBOR provides significant size reduction, I will implement a small number of RPC calls in CBOR,\r\nand compare the uncompressed and compressed sizes against the current JSON encoding.\r\n",
  "closed_by": {
    "login": "adamjonas",
    "id": 755825,
    "node_id": "MDQ6VXNlcjc1NTgyNQ==",
    "avatar_url": "https://avatars.githubusercontent.com/u/755825?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/adamjonas",
    "html_url": "https://github.com/adamjonas",
    "followers_url": "https://api.github.com/users/adamjonas/followers",
    "following_url": "https://api.github.com/users/adamjonas/following{/other_user}",
    "gists_url": "https://api.github.com/users/adamjonas/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/adamjonas/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/adamjonas/subscriptions",
    "organizations_url": "https://api.github.com/users/adamjonas/orgs",
    "repos_url": "https://api.github.com/users/adamjonas/repos",
    "events_url": "https://api.github.com/users/adamjonas/events{/privacy}",
    "received_events_url": "https://api.github.com/users/adamjonas/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22866/timeline",
  "performed_via_github_app": null,
  "state_reason": "not_planned"
}
