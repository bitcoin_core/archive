{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/28970",
  "id": 2016966837,
  "node_id": "PR_kwDOABII585gsIyD",
  "number": 28970,
  "title": "p2p: opportunistically accept 1-parent-1-child packages",
  "user": {
    "login": "glozow",
    "id": 25183001,
    "node_id": "MDQ6VXNlcjI1MTgzMDAx",
    "avatar_url": "https://avatars.githubusercontent.com/u/25183001?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/glozow",
    "html_url": "https://github.com/glozow",
    "followers_url": "https://api.github.com/users/glozow/followers",
    "following_url": "https://api.github.com/users/glozow/following{/other_user}",
    "gists_url": "https://api.github.com/users/glozow/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/glozow/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/glozow/subscriptions",
    "organizations_url": "https://api.github.com/users/glozow/orgs",
    "repos_url": "https://api.github.com/users/glozow/repos",
    "events_url": "https://api.github.com/users/glozow/events{/privacy}",
    "received_events_url": "https://api.github.com/users/glozow/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 98298007,
      "node_id": "MDU6TGFiZWw5ODI5ODAwNw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/P2P",
      "name": "P2P",
      "color": "006b75",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": false,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 28,
  "created_at": "2023-11-29T16:25:29Z",
  "updated_at": "2024-05-01T14:24:40Z",
  "closed_at": "2024-04-30T22:41:08Z",
  "author_association": "MEMBER",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/28970",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/28970",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/28970.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/28970.patch",
    "merged_at": "2024-04-30T22:41:08Z"
  },
  "body_html": "<p dir=\"auto\">This enables 1p1c packages to propagate in the \"happy case\" (i.e. not reliable if there are adversaries) and contains a lot of package relay-related code. See <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1668056618\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/27463\" data-hovercard-type=\"issue\" data-hovercard-url=\"/bitcoin/bitcoin/issues/27463/hovercard\" href=\"https://github.com/bitcoin/bitcoin/issues/27463\">#27463</a> for overall package relay tracking.</p>\n<p dir=\"auto\">Rationale: This is \"non-robust 1-parent-1-child package relay\" which is immediately useful.</p>\n<ul dir=\"auto\">\n<li>Relaying 1-parent-1-child CPFP when mempool min feerate is high would be a subset of all package relay use cases, but a pretty significant improvement over what we have today, where such transactions don't propagate at all. [1]</li>\n<li>Today, a miner can run this with a normal/small maxmempool to get revenue from 1p1c CPFP'd transactions without losing out on the ones with parents below mempool minimum feerate.</li>\n<li>The majority of this code is useful for building more featureful/robust package relay e.g. see the code in <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1724281348\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/27742\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/27742/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/27742\">#27742</a>.</li>\n</ul>\n<p dir=\"auto\">The first 2 commits are followups from <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"2178805781\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/29619\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/29619/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/29619\">#29619</a>:</p>\n<ul dir=\"auto\">\n<li><a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"2178805781\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/29619\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/29619/hovercard?comment_id=1523094034&amp;comment_type=review_comment\" href=\"https://github.com/bitcoin/bitcoin/pull/29619#discussion_r1523094034\">#29619 (comment)</a></li>\n<li><a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"2178805781\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/29619\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/29619/hovercard?comment_id=1519819257&amp;comment_type=review_comment\" href=\"https://github.com/bitcoin/bitcoin/pull/29619#discussion_r1519819257\">#29619 (comment)</a></li>\n</ul>\n<p dir=\"auto\">Q: What makes this short of a more full package relay feature?</p>\n<p dir=\"auto\">(1) it only supports packages in which 1 of the parents needs to be CPFP'd by the child. That includes 1-parent-1-child packages and situations in which the other parents already pay for themselves (and are thus in mempool already when the package is submitted). More general package relay is a future improvement that requires more engineering in mempool and validation - see <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1668056618\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/27463\" data-hovercard-type=\"issue\" data-hovercard-url=\"/bitcoin/bitcoin/issues/27463/hovercard\" href=\"https://github.com/bitcoin/bitcoin/issues/27463\">#27463</a>.</p>\n<p dir=\"auto\">(2) We rely on having kept the child in orphanage, and don't make any attempt to protect it while we wait to receive the parent. If we are experiencing a lot of orphanage churn (e.g. an adversary is purposefully sending us a lot of transactions with missing inputs), we will fail to submit packages. This limitation has been around for 12+ years, see <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1724281348\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/27742\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/27742/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/27742\">#27742</a> which adds a token bucket scheme for protecting package-related orphans at a limited rate per peer.</p>\n<p dir=\"auto\">(3) Our orphan-handling logic is somewhat opportunistic; we don't make much effort to resolve an orphan beyond asking the child's sender for the parents. This means we may miss packages if the first sender fails to give us the parent (intentionally or unintentionally). To make this more robust, we need receiver-side logic to retry orphan resolution with multiple peers. This is also an existing problem which has a proposed solution in <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1789806361\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/28031\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/28031/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/28031\">#28031</a>.</p>\n<p dir=\"auto\">[1]: see this writeup and its links <a href=\"https://github.com/bitcoin/bips/blob/02ec218c7857ef60914e9a3d383b68caf987f70b/bip-0331.mediawiki#propagate-high-feerate-transactions\">https://github.com/bitcoin/bips/blob/02ec218c7857ef60914e9a3d383b68caf987f70b/bip-0331.mediawiki#propagate-high-feerate-transactions</a></p>",
  "body_text": "This enables 1p1c packages to propagate in the \"happy case\" (i.e. not reliable if there are adversaries) and contains a lot of package relay-related code. See #27463 for overall package relay tracking.\nRationale: This is \"non-robust 1-parent-1-child package relay\" which is immediately useful.\n\nRelaying 1-parent-1-child CPFP when mempool min feerate is high would be a subset of all package relay use cases, but a pretty significant improvement over what we have today, where such transactions don't propagate at all. [1]\nToday, a miner can run this with a normal/small maxmempool to get revenue from 1p1c CPFP'd transactions without losing out on the ones with parents below mempool minimum feerate.\nThe majority of this code is useful for building more featureful/robust package relay e.g. see the code in #27742.\n\nThe first 2 commits are followups from #29619:\n\n#29619 (comment)\n#29619 (comment)\n\nQ: What makes this short of a more full package relay feature?\n(1) it only supports packages in which 1 of the parents needs to be CPFP'd by the child. That includes 1-parent-1-child packages and situations in which the other parents already pay for themselves (and are thus in mempool already when the package is submitted). More general package relay is a future improvement that requires more engineering in mempool and validation - see #27463.\n(2) We rely on having kept the child in orphanage, and don't make any attempt to protect it while we wait to receive the parent. If we are experiencing a lot of orphanage churn (e.g. an adversary is purposefully sending us a lot of transactions with missing inputs), we will fail to submit packages. This limitation has been around for 12+ years, see #27742 which adds a token bucket scheme for protecting package-related orphans at a limited rate per peer.\n(3) Our orphan-handling logic is somewhat opportunistic; we don't make much effort to resolve an orphan beyond asking the child's sender for the parents. This means we may miss packages if the first sender fails to give us the parent (intentionally or unintentionally). To make this more robust, we need receiver-side logic to retry orphan resolution with multiple peers. This is also an existing problem which has a proposed solution in #28031.\n[1]: see this writeup and its links https://github.com/bitcoin/bips/blob/02ec218c7857ef60914e9a3d383b68caf987f70b/bip-0331.mediawiki#propagate-high-feerate-transactions",
  "body": "This enables 1p1c packages to propagate in the \"happy case\" (i.e. not reliable if there are adversaries) and contains a lot of package relay-related code. See https://github.com/bitcoin/bitcoin/issues/27463 for overall package relay tracking.\r\n\r\nRationale: This is \"non-robust 1-parent-1-child package relay\" which is immediately useful.\r\n- Relaying 1-parent-1-child CPFP when mempool min feerate is high would be a subset of all package relay use cases, but a pretty significant improvement over what we have today, where such transactions don't propagate at all. [1]\r\n- Today, a miner can run this with a normal/small maxmempool to get revenue from 1p1c CPFP'd transactions without losing out on the ones with parents below mempool minimum feerate.\r\n- The majority of this code is useful for building more featureful/robust package relay e.g. see the code in #27742.\r\n\r\nThe first 2 commits are followups from #29619:\r\n- https://github.com/bitcoin/bitcoin/pull/29619#discussion_r1523094034\r\n- https://github.com/bitcoin/bitcoin/pull/29619#discussion_r1519819257\r\n\r\nQ: What makes this short of a more full package relay feature?\r\n\r\n(1) it only supports packages in which 1 of the parents needs to be CPFP'd by the child. That includes 1-parent-1-child packages and situations in which the other parents already pay for themselves (and are thus in mempool already when the package is submitted). More general package relay is a future improvement that requires more engineering in mempool and validation - see #27463.\r\n\r\n(2) We rely on having kept the child in orphanage, and don't make any attempt to protect it while we wait to receive the parent. If we are experiencing a lot of orphanage churn (e.g. an adversary is purposefully sending us a lot of transactions with missing inputs), we will fail to submit packages. This limitation has been around for 12+ years, see #27742 which adds a token bucket scheme for protecting package-related orphans at a limited rate per peer.\r\n\r\n(3) Our orphan-handling logic is somewhat opportunistic; we don't make much effort to resolve an orphan beyond asking the child's sender for the parents. This means we may miss packages if the first sender fails to give us the parent (intentionally or unintentionally). To make this more robust, we need receiver-side logic to retry orphan resolution with multiple peers. This is also an existing problem which has a proposed solution in #28031.\r\n\r\n[1]: see this writeup and its links https://github.com/bitcoin/bips/blob/02ec218c7857ef60914e9a3d383b68caf987f70b/bip-0331.mediawiki#propagate-high-feerate-transactions",
  "closed_by": {
    "login": "achow101",
    "id": 3782274,
    "node_id": "MDQ6VXNlcjM3ODIyNzQ=",
    "avatar_url": "https://avatars.githubusercontent.com/u/3782274?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/achow101",
    "html_url": "https://github.com/achow101",
    "followers_url": "https://api.github.com/users/achow101/followers",
    "following_url": "https://api.github.com/users/achow101/following{/other_user}",
    "gists_url": "https://api.github.com/users/achow101/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/achow101/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/achow101/subscriptions",
    "organizations_url": "https://api.github.com/users/achow101/orgs",
    "repos_url": "https://api.github.com/users/achow101/repos",
    "events_url": "https://api.github.com/users/achow101/events{/privacy}",
    "received_events_url": "https://api.github.com/users/achow101/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970/reactions",
    "total_count": 5,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 1,
    "rocket": 4,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28970/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
