{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/24115",
  "id": 1109623480,
  "node_id": "PR_kwDOABII584xV090",
  "number": 24115,
  "title": "ARMv8 SHA2 Intrinsics",
  "user": {
    "login": "prusnak",
    "id": 42201,
    "node_id": "MDQ6VXNlcjQyMjAx",
    "avatar_url": "https://avatars.githubusercontent.com/u/42201?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/prusnak",
    "html_url": "https://github.com/prusnak",
    "followers_url": "https://api.github.com/users/prusnak/followers",
    "following_url": "https://api.github.com/users/prusnak/following{/other_user}",
    "gists_url": "https://api.github.com/users/prusnak/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/prusnak/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/prusnak/subscriptions",
    "organizations_url": "https://api.github.com/users/prusnak/orgs",
    "repos_url": "https://api.github.com/users/prusnak/repos",
    "events_url": "https://api.github.com/users/prusnak/events{/privacy}",
    "received_events_url": "https://api.github.com/users/prusnak/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 241832923,
      "node_id": "MDU6TGFiZWwyNDE4MzI5MjM=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Utils/log/libs",
      "name": "Utils/log/libs",
      "color": "5319e7",
      "default": false,
      "description": ""
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/milestones/52",
    "html_url": "https://github.com/bitcoin/bitcoin/milestone/52",
    "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/milestones/52/labels",
    "id": 6179837,
    "node_id": "MDk6TWlsZXN0b25lNjE3OTgzNw==",
    "number": 52,
    "title": "23.0",
    "description": "",
    "creator": {
      "login": "maflcko",
      "id": 6399679,
      "node_id": "MDQ6VXNlcjYzOTk2Nzk=",
      "avatar_url": "https://avatars.githubusercontent.com/u/6399679?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/maflcko",
      "html_url": "https://github.com/maflcko",
      "followers_url": "https://api.github.com/users/maflcko/followers",
      "following_url": "https://api.github.com/users/maflcko/following{/other_user}",
      "gists_url": "https://api.github.com/users/maflcko/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/maflcko/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/maflcko/subscriptions",
      "organizations_url": "https://api.github.com/users/maflcko/orgs",
      "repos_url": "https://api.github.com/users/maflcko/repos",
      "events_url": "https://api.github.com/users/maflcko/events{/privacy}",
      "received_events_url": "https://api.github.com/users/maflcko/received_events",
      "type": "User",
      "site_admin": false
    },
    "open_issues": 0,
    "closed_issues": 123,
    "state": "closed",
    "created_at": "2020-12-04T08:54:20Z",
    "updated_at": "2022-04-25T20:51:00Z",
    "due_on": null,
    "closed_at": "2022-04-22T18:08:30Z"
  },
  "comments": 41,
  "created_at": "2022-01-20T18:17:31Z",
  "updated_at": "2023-02-16T10:13:00Z",
  "closed_at": "2022-02-14T20:13:22Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/24115",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/24115",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/24115.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/24115.patch",
    "merged_at": "2022-02-14T20:13:22Z"
  },
  "body_html": "<p dir=\"auto\">This PR adds support for ARMv8 SHA2 Intrinsics.</p>\n<p dir=\"auto\"><span class=\"issue-keyword tooltipped tooltipped-se\" aria-label=\"This pull request closes issue #13401.\">Fixes</span> <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"329607690\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/13401\" data-hovercard-type=\"issue\" data-hovercard-url=\"/bitcoin/bitcoin/issues/13401/hovercard\" href=\"https://github.com/bitcoin/bitcoin/issues/13401\">#13401</a> and <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"519916830\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/17414\" data-hovercard-type=\"issue\" data-hovercard-url=\"/bitcoin/bitcoin/issues/17414/hovercard\" href=\"https://github.com/bitcoin/bitcoin/issues/17414\">#17414</a></p>\n<ul dir=\"auto\">\n<li>Integration part was done by me.</li>\n<li>The original SHA2 NI code comes from <a href=\"https://github.com/noloader/SHA-Intrinsics/blob/master/sha256-arm.c\">https://github.com/noloader/SHA-Intrinsics/blob/master/sha256-arm.c</a></li>\n<li>Minor optimizations from <a href=\"https://github.com/rollmeister/bitcoin-armv8/blob/master/src/crypto/sha256.cpp\">https://github.com/rollmeister/bitcoin-armv8/blob/master/src/crypto/sha256.cpp</a> are applied too.</li>\n<li>The 2-way transform added by <a class=\"user-mention notranslate\" data-hovercard-type=\"user\" data-hovercard-url=\"/users/sipa/hovercard\" data-octo-click=\"hovercard-link-click\" data-octo-dimensions=\"link_type:self\" href=\"https://github.com/sipa\">@sipa</a></li>\n</ul>",
  "body_text": "This PR adds support for ARMv8 SHA2 Intrinsics.\nFixes #13401 and #17414\n\nIntegration part was done by me.\nThe original SHA2 NI code comes from https://github.com/noloader/SHA-Intrinsics/blob/master/sha256-arm.c\nMinor optimizations from https://github.com/rollmeister/bitcoin-armv8/blob/master/src/crypto/sha256.cpp are applied too.\nThe 2-way transform added by @sipa",
  "body": "This PR adds support for ARMv8 SHA2 Intrinsics.\r\n\r\nFixes https://github.com/bitcoin/bitcoin/issues/13401 and https://github.com/bitcoin/bitcoin/issues/17414\r\n\r\n* Integration part was done by me.\r\n* The original SHA2 NI code comes from https://github.com/noloader/SHA-Intrinsics/blob/master/sha256-arm.c\r\n* Minor optimizations from https://github.com/rollmeister/bitcoin-armv8/blob/master/src/crypto/sha256.cpp are applied too.\r\n* The 2-way transform added by @sipa ",
  "closed_by": {
    "login": "laanwj",
    "id": 126646,
    "node_id": "MDQ6VXNlcjEyNjY0Ng==",
    "avatar_url": "https://avatars.githubusercontent.com/u/126646?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/laanwj",
    "html_url": "https://github.com/laanwj",
    "followers_url": "https://api.github.com/users/laanwj/followers",
    "following_url": "https://api.github.com/users/laanwj/following{/other_user}",
    "gists_url": "https://api.github.com/users/laanwj/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/laanwj/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/laanwj/subscriptions",
    "organizations_url": "https://api.github.com/users/laanwj/orgs",
    "repos_url": "https://api.github.com/users/laanwj/repos",
    "events_url": "https://api.github.com/users/laanwj/events{/privacy}",
    "received_events_url": "https://api.github.com/users/laanwj/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115/reactions",
    "total_count": 11,
    "+1": 1,
    "-1": 0,
    "laugh": 0,
    "hooray": 1,
    "confused": 0,
    "heart": 1,
    "rocket": 6,
    "eyes": 2
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24115/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
