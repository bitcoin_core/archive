{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/28523",
  "id": 1909949345,
  "node_id": "PR_kwDOABII585bC3L1",
  "number": 28523,
  "title": "rpc: add hidden getrawaddrman RPC to list addrman table entries",
  "user": {
    "login": "0xB10C",
    "id": 19157360,
    "node_id": "MDQ6VXNlcjE5MTU3MzYw",
    "avatar_url": "https://avatars.githubusercontent.com/u/19157360?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/0xB10C",
    "html_url": "https://github.com/0xB10C",
    "followers_url": "https://api.github.com/users/0xB10C/followers",
    "following_url": "https://api.github.com/users/0xB10C/following{/other_user}",
    "gists_url": "https://api.github.com/users/0xB10C/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/0xB10C/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/0xB10C/subscriptions",
    "organizations_url": "https://api.github.com/users/0xB10C/orgs",
    "repos_url": "https://api.github.com/users/0xB10C/repos",
    "events_url": "https://api.github.com/users/0xB10C/events{/privacy}",
    "received_events_url": "https://api.github.com/users/0xB10C/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 98279177,
      "node_id": "MDU6TGFiZWw5ODI3OTE3Nw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/RPC/REST/ZMQ",
      "name": "RPC/REST/ZMQ",
      "color": "0052cc",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": false,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 18,
  "created_at": "2023-09-23T17:48:13Z",
  "updated_at": "2023-10-18T09:05:23Z",
  "closed_at": "2023-10-03T15:38:37Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/28523",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/28523",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/28523.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/28523.patch",
    "merged_at": "2023-10-03T15:38:36Z"
  },
  "body_html": "<p dir=\"auto\">Inspired by <code class=\"notranslate\">getaddrmaninfo</code> (<a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"1678790809\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/27511\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/27511/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/27511\">#27511</a>), this adds a hidden/test-only <code class=\"notranslate\">getrawaddrman</code> RPC. The RPC returns information on all addresses in the address manager new and tried tables. Addrman table contents can be used in tests and during development.</p>\n<p dir=\"auto\">The RPC result encodes the <code class=\"notranslate\">bucket</code> and <code class=\"notranslate\">position</code>, the internal location of addresses in the tables, in the address object's string key. This allows users to choose to consume or to ignore the location information. If the internals of the address manager implementation change, the location encoding might change too.</p>\n<div class=\"snippet-clipboard-content notranslate position-relative overflow-auto\" data-snippet-clipboard-copy-content=\"getrawaddrman\n\nEXPERIMENTAL warning: this call may be changed in future releases.\n\nReturns information on all address manager entries for the new and tried tables.\n\nResult:\n{                                  (json object)\n  &quot;table&quot; : {                      (json object) buckets with addresses in the address manager table ( new, tried )\n    &quot;bucket/position&quot; : {          (json object) the location in the address manager table (&lt;bucket&gt;/&lt;position&gt;)\n      &quot;address&quot; : &quot;str&quot;,           (string) The address of the node\n      &quot;port&quot; : n,                  (numeric) The port number of the node\n      &quot;network&quot; : &quot;str&quot;,           (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the address\n      &quot;services&quot; : n,              (numeric) The services offered by the node\n      &quot;time&quot; : xxx,                (numeric) The UNIX epoch time when the node was last seen\n      &quot;source&quot; : &quot;str&quot;,            (string) The address that relayed the address to us\n      &quot;source_network&quot; : &quot;str&quot;     (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the source address\n    },\n    ...\n  },\n  ...\n}\n\nExamples:\n&gt; bitcoin-cli getrawaddrman\n&gt; curl --user myusername --data-binary '{&quot;jsonrpc&quot;: &quot;1.0&quot;, &quot;id&quot;: &quot;curltest&quot;, &quot;method&quot;: &quot;getrawaddrman&quot;, &quot;params&quot;: []}' -H 'content-type: text/plain;' http://127.0.0.1:8332/\"><pre class=\"notranslate\"><code class=\"notranslate\">getrawaddrman\n\nEXPERIMENTAL warning: this call may be changed in future releases.\n\nReturns information on all address manager entries for the new and tried tables.\n\nResult:\n{                                  (json object)\n  \"table\" : {                      (json object) buckets with addresses in the address manager table ( new, tried )\n    \"bucket/position\" : {          (json object) the location in the address manager table (&lt;bucket&gt;/&lt;position&gt;)\n      \"address\" : \"str\",           (string) The address of the node\n      \"port\" : n,                  (numeric) The port number of the node\n      \"network\" : \"str\",           (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the address\n      \"services\" : n,              (numeric) The services offered by the node\n      \"time\" : xxx,                (numeric) The UNIX epoch time when the node was last seen\n      \"source\" : \"str\",            (string) The address that relayed the address to us\n      \"source_network\" : \"str\"     (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the source address\n    },\n    ...\n  },\n  ...\n}\n\nExamples:\n&gt; bitcoin-cli getrawaddrman\n&gt; curl --user myusername --data-binary '{\"jsonrpc\": \"1.0\", \"id\": \"curltest\", \"method\": \"getrawaddrman\", \"params\": []}' -H 'content-type: text/plain;' http://127.0.0.1:8332/\n</code></pre></div>",
  "body_text": "Inspired by getaddrmaninfo (#27511), this adds a hidden/test-only getrawaddrman RPC. The RPC returns information on all addresses in the address manager new and tried tables. Addrman table contents can be used in tests and during development.\nThe RPC result encodes the bucket and position, the internal location of addresses in the tables, in the address object's string key. This allows users to choose to consume or to ignore the location information. If the internals of the address manager implementation change, the location encoding might change too.\ngetrawaddrman\n\nEXPERIMENTAL warning: this call may be changed in future releases.\n\nReturns information on all address manager entries for the new and tried tables.\n\nResult:\n{                                  (json object)\n  \"table\" : {                      (json object) buckets with addresses in the address manager table ( new, tried )\n    \"bucket/position\" : {          (json object) the location in the address manager table (<bucket>/<position>)\n      \"address\" : \"str\",           (string) The address of the node\n      \"port\" : n,                  (numeric) The port number of the node\n      \"network\" : \"str\",           (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the address\n      \"services\" : n,              (numeric) The services offered by the node\n      \"time\" : xxx,                (numeric) The UNIX epoch time when the node was last seen\n      \"source\" : \"str\",            (string) The address that relayed the address to us\n      \"source_network\" : \"str\"     (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the source address\n    },\n    ...\n  },\n  ...\n}\n\nExamples:\n> bitcoin-cli getrawaddrman\n> curl --user myusername --data-binary '{\"jsonrpc\": \"1.0\", \"id\": \"curltest\", \"method\": \"getrawaddrman\", \"params\": []}' -H 'content-type: text/plain;' http://127.0.0.1:8332/",
  "body": "Inspired by `getaddrmaninfo` (#27511), this adds a hidden/test-only `getrawaddrman` RPC. The RPC returns information on all addresses in the address manager new and tried tables. Addrman table contents can be used in tests and during development. \r\n\r\nThe RPC result encodes the `bucket` and `position`, the internal location of addresses in the tables, in the address object's string key. This allows users to choose to consume or to ignore the location information. If the internals of the address manager implementation change, the location encoding might change too.  \r\n\r\n```\r\ngetrawaddrman\r\n\r\nEXPERIMENTAL warning: this call may be changed in future releases.\r\n\r\nReturns information on all address manager entries for the new and tried tables.\r\n\r\nResult:\r\n{                                  (json object)\r\n  \"table\" : {                      (json object) buckets with addresses in the address manager table ( new, tried )\r\n    \"bucket/position\" : {          (json object) the location in the address manager table (<bucket>/<position>)\r\n      \"address\" : \"str\",           (string) The address of the node\r\n      \"port\" : n,                  (numeric) The port number of the node\r\n      \"network\" : \"str\",           (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the address\r\n      \"services\" : n,              (numeric) The services offered by the node\r\n      \"time\" : xxx,                (numeric) The UNIX epoch time when the node was last seen\r\n      \"source\" : \"str\",            (string) The address that relayed the address to us\r\n      \"source_network\" : \"str\"     (string) The network (ipv4, ipv6, onion, i2p, cjdns) of the source address\r\n    },\r\n    ...\r\n  },\r\n  ...\r\n}\r\n\r\nExamples:\r\n> bitcoin-cli getrawaddrman\r\n> curl --user myusername --data-binary '{\"jsonrpc\": \"1.0\", \"id\": \"curltest\", \"method\": \"getrawaddrman\", \"params\": []}' -H 'content-type: text/plain;' http://127.0.0.1:8332/\r\n```\r\n",
  "closed_by": {
    "login": "achow101",
    "id": 3782274,
    "node_id": "MDQ6VXNlcjM3ODIyNzQ=",
    "avatar_url": "https://avatars.githubusercontent.com/u/3782274?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/achow101",
    "html_url": "https://github.com/achow101",
    "followers_url": "https://api.github.com/users/achow101/followers",
    "following_url": "https://api.github.com/users/achow101/following{/other_user}",
    "gists_url": "https://api.github.com/users/achow101/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/achow101/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/achow101/subscriptions",
    "organizations_url": "https://api.github.com/users/achow101/orgs",
    "repos_url": "https://api.github.com/users/achow101/repos",
    "events_url": "https://api.github.com/users/achow101/events{/privacy}",
    "received_events_url": "https://api.github.com/users/achow101/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523/reactions",
    "total_count": 8,
    "+1": 2,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 2,
    "rocket": 4,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/28523/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
