{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/22787",
  "id": 977689753,
  "node_id": "MDExOlB1bGxSZXF1ZXN0NzE4Mzk3NzA4",
  "number": 22787,
  "title": "refactor: actual immutable pointing",
  "user": {
    "login": "kallewoof",
    "id": 250224,
    "node_id": "MDQ6VXNlcjI1MDIyNA==",
    "avatar_url": "https://avatars.githubusercontent.com/u/250224?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kallewoof",
    "html_url": "https://github.com/kallewoof",
    "followers_url": "https://api.github.com/users/kallewoof/followers",
    "following_url": "https://api.github.com/users/kallewoof/following{/other_user}",
    "gists_url": "https://api.github.com/users/kallewoof/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kallewoof/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kallewoof/subscriptions",
    "organizations_url": "https://api.github.com/users/kallewoof/orgs",
    "repos_url": "https://api.github.com/users/kallewoof/repos",
    "events_url": "https://api.github.com/users/kallewoof/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kallewoof/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 135961,
      "node_id": "MDU6TGFiZWwxMzU5NjE=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Refactoring",
      "name": "Refactoring",
      "color": "E6F6D6",
      "default": false,
      "description": null
    },
    {
      "id": 149424,
      "node_id": "MDU6TGFiZWwxNDk0MjQ=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Wallet",
      "name": "Wallet",
      "color": "08a781",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 15,
  "created_at": "2021-08-24T04:24:55Z",
  "updated_at": "2022-10-30T19:09:32Z",
  "closed_at": "2021-10-29T08:53:56Z",
  "author_association": "MEMBER",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/22787",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/22787",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/22787.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/22787.patch",
    "merged_at": "2021-10-29T08:53:56Z"
  },
  "body_html": "<div class=\"highlight highlight-source-c++ notranslate position-relative overflow-auto\" dir=\"auto\" data-snippet-clipboard-copy-content=\"const std::shared_ptr&lt;CWallet&gt; wallet = x;\"><pre class=\"notranslate\"><span class=\"pl-k\">const</span> std::shared_ptr&lt;CWallet&gt; wallet = x;</pre></div>\n<p dir=\"auto\">means we can not do <code class=\"notranslate\">wallet = y</code>, but we can totally do <code class=\"notranslate\">wallet-&gt;DestructiveOperation()</code>, contrary to what that line looks like.</p>\n<p dir=\"auto\">This PR</p>\n<ul dir=\"auto\">\n<li>introduces a new convention: always use const shared pointers to <code class=\"notranslate\">CWallet</code>s (even when we mutate the pointed-to thing)</li>\n<li>uses <code class=\"notranslate\">const shared_ptr&lt;const CWallet&gt;</code> everywhere where wallets are not modified</li>\n</ul>\n<p dir=\"auto\">In the future, this should preferably apply to all shared pointers, not limited to just <code class=\"notranslate\">CWallet</code>s.</p>\n<p dir=\"auto\">Both of these serve the same purpose: to dispell the misconception that <code class=\"notranslate\">const shared_ptr&lt;X&gt;</code> immutates <code class=\"notranslate\">X</code>. It doesn't, and it's dangerous to leave this misconception as is, for obvious reasons.</p>",
  "body_text": "const std::shared_ptr<CWallet> wallet = x;\nmeans we can not do wallet = y, but we can totally do wallet->DestructiveOperation(), contrary to what that line looks like.\nThis PR\n\nintroduces a new convention: always use const shared pointers to CWallets (even when we mutate the pointed-to thing)\nuses const shared_ptr<const CWallet> everywhere where wallets are not modified\n\nIn the future, this should preferably apply to all shared pointers, not limited to just CWallets.\nBoth of these serve the same purpose: to dispell the misconception that const shared_ptr<X> immutates X. It doesn't, and it's dangerous to leave this misconception as is, for obvious reasons.",
  "body": "```C++\r\nconst std::shared_ptr<CWallet> wallet = x;\r\n```\r\nmeans we can not do `wallet = y`, but we can totally do `wallet->DestructiveOperation()`, contrary to what that line looks like.\r\n\r\nThis PR\r\n\r\n* introduces a new convention: always use const shared pointers to `CWallet`s (even when we mutate the pointed-to thing)\r\n* uses `const shared_ptr<const CWallet>` everywhere where wallets are not modified\r\n\r\nIn the future, this should preferably apply to all shared pointers, not limited to just `CWallet`s.\r\n\r\nBoth of these serve the same purpose: to dispell the misconception that `const shared_ptr<X>` immutates `X`. It doesn't, and it's dangerous to leave this misconception as is, for obvious reasons.",
  "closed_by": {
    "login": "maflcko",
    "id": 6399679,
    "node_id": "MDQ6VXNlcjYzOTk2Nzk=",
    "avatar_url": "https://avatars.githubusercontent.com/u/6399679?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/maflcko",
    "html_url": "https://github.com/maflcko",
    "followers_url": "https://api.github.com/users/maflcko/followers",
    "following_url": "https://api.github.com/users/maflcko/following{/other_user}",
    "gists_url": "https://api.github.com/users/maflcko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/maflcko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/maflcko/subscriptions",
    "organizations_url": "https://api.github.com/users/maflcko/orgs",
    "repos_url": "https://api.github.com/users/maflcko/repos",
    "events_url": "https://api.github.com/users/maflcko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/maflcko/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787/reactions",
    "total_count": 5,
    "+1": 5,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/22787/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
