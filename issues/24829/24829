{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829/events",
  "html_url": "https://github.com/bitcoin/bitcoin/issues/24829",
  "id": 1200473088,
  "node_id": "I_kwDOABII585HjcQA",
  "number": 24829,
  "title": "Export a watch wallet only (with descriptors and without private keys) for an air gap setup",
  "user": {
    "login": "Tracachang",
    "id": 54238558,
    "node_id": "MDQ6VXNlcjU0MjM4NTU4",
    "avatar_url": "https://avatars.githubusercontent.com/u/54238558?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/Tracachang",
    "html_url": "https://github.com/Tracachang",
    "followers_url": "https://api.github.com/users/Tracachang/followers",
    "following_url": "https://api.github.com/users/Tracachang/following{/other_user}",
    "gists_url": "https://api.github.com/users/Tracachang/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/Tracachang/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/Tracachang/subscriptions",
    "organizations_url": "https://api.github.com/users/Tracachang/orgs",
    "repos_url": "https://api.github.com/users/Tracachang/repos",
    "events_url": "https://api.github.com/users/Tracachang/events{/privacy}",
    "received_events_url": "https://api.github.com/users/Tracachang/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 64583,
      "node_id": "MDU6TGFiZWw2NDU4Mw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Feature",
      "name": "Feature",
      "color": "7cf575",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 12,
  "created_at": "2022-04-11T20:05:33Z",
  "updated_at": "2024-05-01T12:45:51Z",
  "closed_at": "2023-05-01T17:54:53Z",
  "author_association": "NONE",
  "active_lock_reason": null,
  "body_html": "<p dir=\"auto\"><strong>Is your feature request related to a problem? Please describe.</strong></p>\n\n<p dir=\"auto\">For long time I've seen users looking for a way to do an air gap setup with Bitcoin Core, since there were no easy way people tend to go with other solutions such as armory, electrum which offers an easy and friendly way to do it.</p>\n<p dir=\"auto\">Since Bitcoin Core v 22.0 and with the command  listdescriptors it can be done easily:</p>\n<p dir=\"auto\">OFFLINE PC - Create a wallet with descriptors=true, export descriptors with \"listdescriptors\".<br>\nONLINE PC - Create a wallet without privatekeys, descriptors=true, and importdescriptors to have a functional watch only wallet for receiving funds and create unsigned transactions.</p>\n<p dir=\"auto\"><strong>Describe the solution you'd like</strong></p>\n\n<p dir=\"auto\">Instead of creating two wallets, one offline and a watch only and manually have to export/import descriptors, the wallet containing the private keys could offer an option like \"exportwatchonly\" that would generate a \"watch_wallet.dat\" without private keys and all descriptors already imported. Making very easy and user friendly the process to create an air gap setup.</p>",
  "body_text": "Is your feature request related to a problem? Please describe.\n\nFor long time I've seen users looking for a way to do an air gap setup with Bitcoin Core, since there were no easy way people tend to go with other solutions such as armory, electrum which offers an easy and friendly way to do it.\nSince Bitcoin Core v 22.0 and with the command  listdescriptors it can be done easily:\nOFFLINE PC - Create a wallet with descriptors=true, export descriptors with \"listdescriptors\".\nONLINE PC - Create a wallet without privatekeys, descriptors=true, and importdescriptors to have a functional watch only wallet for receiving funds and create unsigned transactions.\nDescribe the solution you'd like\n\nInstead of creating two wallets, one offline and a watch only and manually have to export/import descriptors, the wallet containing the private keys could offer an option like \"exportwatchonly\" that would generate a \"watch_wallet.dat\" without private keys and all descriptors already imported. Making very easy and user friendly the process to create an air gap setup.",
  "body": "**Is your feature request related to a problem? Please describe.**\r\n<!-- A clear and concise description of what the problem is. Ex. I'm always frustrated when [...] -->\r\nFor long time I've seen users looking for a way to do an air gap setup with Bitcoin Core, since there were no easy way people tend to go with other solutions such as armory, electrum which offers an easy and friendly way to do it.\r\n\r\nSince Bitcoin Core v 22.0 and with the command  listdescriptors it can be done easily:\r\n\r\nOFFLINE PC - Create a wallet with descriptors=true, export descriptors with \"listdescriptors\".\r\nONLINE PC - Create a wallet without privatekeys, descriptors=true, and importdescriptors to have a functional watch only wallet for receiving funds and create unsigned transactions.\r\n\r\n**Describe the solution you'd like**\r\n<!-- A clear and concise description of what you want to happen. -->\r\n\r\nInstead of creating two wallets, one offline and a watch only and manually have to export/import descriptors, the wallet containing the private keys could offer an option like \"exportwatchonly\" that would generate a \"watch_wallet.dat\" without private keys and all descriptors already imported. Making very easy and user friendly the process to create an air gap setup.",
  "closed_by": {
    "login": "pinheadmz",
    "id": 2084648,
    "node_id": "MDQ6VXNlcjIwODQ2NDg=",
    "avatar_url": "https://avatars.githubusercontent.com/u/2084648?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pinheadmz",
    "html_url": "https://github.com/pinheadmz",
    "followers_url": "https://api.github.com/users/pinheadmz/followers",
    "following_url": "https://api.github.com/users/pinheadmz/following{/other_user}",
    "gists_url": "https://api.github.com/users/pinheadmz/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pinheadmz/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pinheadmz/subscriptions",
    "organizations_url": "https://api.github.com/users/pinheadmz/orgs",
    "repos_url": "https://api.github.com/users/pinheadmz/repos",
    "events_url": "https://api.github.com/users/pinheadmz/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pinheadmz/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829/reactions",
    "total_count": 6,
    "+1": 6,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/24829/timeline",
  "performed_via_github_app": null,
  "state_reason": "completed"
}
