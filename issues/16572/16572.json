{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/16572",
  "id": 478731965,
  "node_id": "MDExOlB1bGxSZXF1ZXN0MzA1Nzg1OTI2",
  "number": 16572,
  "title": "wallet: Fix Char as Bool in Wallet",
  "user": {
    "login": "JeremyRubin",
    "id": 886523,
    "node_id": "MDQ6VXNlcjg4NjUyMw==",
    "avatar_url": "https://avatars.githubusercontent.com/u/886523?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/JeremyRubin",
    "html_url": "https://github.com/JeremyRubin",
    "followers_url": "https://api.github.com/users/JeremyRubin/followers",
    "following_url": "https://api.github.com/users/JeremyRubin/following{/other_user}",
    "gists_url": "https://api.github.com/users/JeremyRubin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/JeremyRubin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/JeremyRubin/subscriptions",
    "organizations_url": "https://api.github.com/users/JeremyRubin/orgs",
    "repos_url": "https://api.github.com/users/JeremyRubin/repos",
    "events_url": "https://api.github.com/users/JeremyRubin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/JeremyRubin/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 149424,
      "node_id": "MDU6TGFiZWwxNDk0MjQ=",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/Wallet",
      "name": "Wallet",
      "color": "08a781",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 17,
  "created_at": "2019-08-08T23:36:52Z",
  "updated_at": "2021-12-16T14:57:36Z",
  "closed_at": "2019-08-21T07:27:10Z",
  "author_association": "CONTRIBUTOR",
  "active_lock_reason": "resolved",
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/16572",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/16572",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/16572.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/16572.patch",
    "merged_at": "2019-08-21T07:27:10Z"
  },
  "body_html": "<p dir=\"auto\">In a few places in src/wallet/wallet.h, we use a char when semantically we want a bool.</p>\n<p dir=\"auto\">This is kind of an issue because it means we can unserialize the same transaction with different fFromMe flags (as differing chars) and evaluate the following section in wallet/wallet.cpp</p>\n<div class=\"highlight highlight-source-c++ notranslate position-relative overflow-auto\" dir=\"auto\" data-snippet-clipboard-copy-content=\"        if (wtxIn.fFromMe &amp;&amp; wtxIn.fFromMe != wtx.fFromMe)\n         {\n             wtx.fFromMe = wtxIn.fFromMe;\n             fUpdated = true;\n         }\"><pre class=\"notranslate\">        <span class=\"pl-k\">if</span> (wtxIn.<span class=\"pl-smi\">fFromMe</span> &amp;&amp; wtxIn.<span class=\"pl-smi\">fFromMe</span> != wtx.<span class=\"pl-smi\">fFromMe</span>)\n         {\n             wtx.<span class=\"pl-smi\">fFromMe</span> = wtxIn.<span class=\"pl-smi\">fFromMe</span>;\n             <span class=\"pl-smi\">fUpdated</span> = <span class=\"pl-c1\">true</span>;\n         }</pre></div>\n<p dir=\"auto\">incorrectly (triggering an fUpdated where both fFromMe values represent true, via different chars).</p>\n<p dir=\"auto\">I don't think this is a vulnerability, but it's just a little messy and unsemantic, and could lead to issues with stored wtxIns not being findable in a map by their hash.</p>\n<p dir=\"auto\">The serialize/unserialize code for bool internally uses a char, so it should be safe to make this substitution.</p>\n<p dir=\"auto\">NOTE: Technically, this is a behavior change -- I haven't checked too closely that nowhere is depending on storing information in this char. Theoretically, this could break something because after this change a tx unserialized with such a char would preserve it's value, but now it is converted to a <del>true</del> canonical bool.</p>",
  "body_text": "In a few places in src/wallet/wallet.h, we use a char when semantically we want a bool.\nThis is kind of an issue because it means we can unserialize the same transaction with different fFromMe flags (as differing chars) and evaluate the following section in wallet/wallet.cpp\n        if (wtxIn.fFromMe && wtxIn.fFromMe != wtx.fFromMe)\n         {\n             wtx.fFromMe = wtxIn.fFromMe;\n             fUpdated = true;\n         }\nincorrectly (triggering an fUpdated where both fFromMe values represent true, via different chars).\nI don't think this is a vulnerability, but it's just a little messy and unsemantic, and could lead to issues with stored wtxIns not being findable in a map by their hash.\nThe serialize/unserialize code for bool internally uses a char, so it should be safe to make this substitution.\nNOTE: Technically, this is a behavior change -- I haven't checked too closely that nowhere is depending on storing information in this char. Theoretically, this could break something because after this change a tx unserialized with such a char would preserve it's value, but now it is converted to a true canonical bool.",
  "body": "In a few places in src/wallet/wallet.h, we use a char when semantically we want a bool.\r\n\r\nThis is kind of an issue because it means we can unserialize the same transaction with different fFromMe flags (as differing chars) and evaluate the following section in wallet/wallet.cpp\r\n```c++\r\n        if (wtxIn.fFromMe && wtxIn.fFromMe != wtx.fFromMe)\r\n         {\r\n             wtx.fFromMe = wtxIn.fFromMe;\r\n             fUpdated = true;\r\n         }\r\n```\r\nincorrectly (triggering an fUpdated where both fFromMe values represent true, via different chars).\r\n\r\nI don't think this is a vulnerability, but it's just a little messy and unsemantic, and could lead to issues with stored wtxIns not being findable in a map by their hash.\r\n\r\nThe serialize/unserialize code for bool internally uses a char, so it should be safe to make this substitution.\r\n\r\n\r\n\r\n\r\nNOTE: Technically, this is a behavior change -- I haven't checked too closely that nowhere is depending on storing information in this char. Theoretically, this could break something because after this change a tx unserialized with such a char would preserve it's value, but now it is converted to a ~true~ canonical bool.",
  "closed_by": {
    "login": "fanquake",
    "id": 863730,
    "node_id": "MDQ6VXNlcjg2MzczMA==",
    "avatar_url": "https://avatars.githubusercontent.com/u/863730?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/fanquake",
    "html_url": "https://github.com/fanquake",
    "followers_url": "https://api.github.com/users/fanquake/followers",
    "following_url": "https://api.github.com/users/fanquake/following{/other_user}",
    "gists_url": "https://api.github.com/users/fanquake/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/fanquake/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/fanquake/subscriptions",
    "organizations_url": "https://api.github.com/users/fanquake/orgs",
    "repos_url": "https://api.github.com/users/fanquake/repos",
    "events_url": "https://api.github.com/users/fanquake/events{/privacy}",
    "received_events_url": "https://api.github.com/users/fanquake/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572/reactions",
    "total_count": 0,
    "+1": 0,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 0,
    "heart": 0,
    "rocket": 0,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/16572/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
