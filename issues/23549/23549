{
  "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549",
  "repository_url": "https://api.github.com/repos/bitcoin/bitcoin",
  "labels_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549/labels{/name}",
  "comments_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549/comments",
  "events_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549/events",
  "html_url": "https://github.com/bitcoin/bitcoin/pull/23549",
  "id": 1057850480,
  "node_id": "PR_kwDOABII584uvS9g",
  "number": 23549,
  "title": "Add scanblocks RPC call (attempt 2)",
  "user": {
    "login": "jamesob",
    "id": 73197,
    "node_id": "MDQ6VXNlcjczMTk3",
    "avatar_url": "https://avatars.githubusercontent.com/u/73197?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jamesob",
    "html_url": "https://github.com/jamesob",
    "followers_url": "https://api.github.com/users/jamesob/followers",
    "following_url": "https://api.github.com/users/jamesob/following{/other_user}",
    "gists_url": "https://api.github.com/users/jamesob/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jamesob/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jamesob/subscriptions",
    "organizations_url": "https://api.github.com/users/jamesob/orgs",
    "repos_url": "https://api.github.com/users/jamesob/repos",
    "events_url": "https://api.github.com/users/jamesob/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jamesob/received_events",
    "type": "User",
    "site_admin": false
  },
  "labels": [
    {
      "id": 98279177,
      "node_id": "MDU6TGFiZWw5ODI3OTE3Nw==",
      "url": "https://api.github.com/repos/bitcoin/bitcoin/labels/RPC/REST/ZMQ",
      "name": "RPC/REST/ZMQ",
      "color": "0052cc",
      "default": false,
      "description": null
    }
  ],
  "state": "closed",
  "locked": true,
  "assignee": null,
  "assignees": [

  ],
  "milestone": null,
  "comments": 21,
  "created_at": "2021-11-18T21:56:52Z",
  "updated_at": "2023-10-13T12:14:35Z",
  "closed_at": "2022-10-13T14:49:29Z",
  "author_association": "MEMBER",
  "active_lock_reason": null,
  "draft": false,
  "pull_request": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/pulls/23549",
    "html_url": "https://github.com/bitcoin/bitcoin/pull/23549",
    "diff_url": "https://github.com/bitcoin/bitcoin/pull/23549.diff",
    "patch_url": "https://github.com/bitcoin/bitcoin/pull/23549.patch",
    "merged_at": "2022-10-13T14:49:29Z"
  },
  "body_html": "<p dir=\"auto\">Revives <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"768097815\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/20664\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/20664/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/20664\">#20664</a>. All feedback from the previous PR has either been responded to inline or incorporated here.</p>\n<hr>\n<p dir=\"auto\">Major changes from Jonas' PR:</p>\n<ul dir=\"auto\">\n<li>consolidated arguments for scantxoutset/scanblocks</li>\n<li>substantial cleanup of the functional test</li>\n</ul>\n<p dir=\"auto\">Here's the range-diff (<code class=\"notranslate\">git range-diff master jonasschnelli/2020/12/filterblocks_rpc jamesob/2021-11-scanblocks</code>): <a href=\"https://gist.github.com/jamesob/aa4a975344209f0316444b8de2ec1d18\">https://gist.github.com/jamesob/aa4a975344209f0316444b8de2ec1d18</a></p>\n<h3 dir=\"auto\">Original PR description</h3>\n<blockquote>\n<p dir=\"auto\">The <code class=\"notranslate\">scanblocks</code> RPC call allows one to get relevant blockhashes from a set of descriptors by scanning all blockfilters in a given range.</p>\n<p dir=\"auto\"><strong>Example:</strong></p>\n<p dir=\"auto\"><code class=\"notranslate\">scanblocks start '[\"addr(&lt;bitcoin_address&gt;)\"]' 661000</code> (returns relevant blockhashes for <code class=\"notranslate\">&lt;bitcoin_address&gt;</code> from blockrange 661000-&gt;tip)</p>\n<h2 dir=\"auto\">Why is this useful?</h2>\n<p dir=\"auto\"><strong>Fast wallet rescans</strong>: get the relevant blocks and only rescan those via <code class=\"notranslate\">rescanblockchain getblockheader(&lt;hash&gt;)[height] getblockheader(&lt;hash&gt;)[height])</code>. A future PR may add an option to allow to provide an array of blockhashes to <code class=\"notranslate\">rescanblockchain</code>.</p>\n<p dir=\"auto\"><strong>prune wallet rescans</strong>: (<em>needs additional changes</em>): together with a call to fetch blocks from the p2p network if they have been pruned, it would allow to rescan wallets back to the genesis block in pruned mode (relevant <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"440000738\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/15946\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/15946/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/15946\">#15946</a>).</p>\n<p dir=\"auto\"><strong>SPV mode</strong> (<em>needs additional changes</em>): it would be possible to build the blockfilterindex from the p2p network (rather then deriving them from the blocks) and thus allow some sort of hybrid-SPV mode with moderate bandwidth consumption (related <a class=\"issue-link js-issue-link\" data-error-text=\"Failed to load title\" data-id=\"199236636\" data-permission-text=\"Title is private\" data-url=\"https://github.com/bitcoin/bitcoin/issues/9483\" data-hovercard-type=\"pull_request\" data-hovercard-url=\"/bitcoin/bitcoin/pull/9483/hovercard\" href=\"https://github.com/bitcoin/bitcoin/pull/9483\">#9483</a>)</p>\n</blockquote>",
  "body_text": "Revives #20664. All feedback from the previous PR has either been responded to inline or incorporated here.\n\nMajor changes from Jonas' PR:\n\nconsolidated arguments for scantxoutset/scanblocks\nsubstantial cleanup of the functional test\n\nHere's the range-diff (git range-diff master jonasschnelli/2020/12/filterblocks_rpc jamesob/2021-11-scanblocks): https://gist.github.com/jamesob/aa4a975344209f0316444b8de2ec1d18\nOriginal PR description\n\nThe scanblocks RPC call allows one to get relevant blockhashes from a set of descriptors by scanning all blockfilters in a given range.\nExample:\nscanblocks start '[\"addr(<bitcoin_address>)\"]' 661000 (returns relevant blockhashes for <bitcoin_address> from blockrange 661000->tip)\nWhy is this useful?\nFast wallet rescans: get the relevant blocks and only rescan those via rescanblockchain getblockheader(<hash>)[height] getblockheader(<hash>)[height]). A future PR may add an option to allow to provide an array of blockhashes to rescanblockchain.\nprune wallet rescans: (needs additional changes): together with a call to fetch blocks from the p2p network if they have been pruned, it would allow to rescan wallets back to the genesis block in pruned mode (relevant #15946).\nSPV mode (needs additional changes): it would be possible to build the blockfilterindex from the p2p network (rather then deriving them from the blocks) and thus allow some sort of hybrid-SPV mode with moderate bandwidth consumption (related #9483)",
  "body": "Revives #20664. All feedback from the previous PR has either been responded to inline or incorporated here.\r\n\r\n---\r\n\r\nMajor changes from Jonas' PR:\r\n- consolidated arguments for scantxoutset/scanblocks\r\n- substantial cleanup of the functional test\r\n\r\nHere's the range-diff (`git range-diff master jonasschnelli/2020/12/filterblocks_rpc jamesob/2021-11-scanblocks`): https://gist.github.com/jamesob/aa4a975344209f0316444b8de2ec1d18\r\n\r\n### Original PR description\r\n\r\n> The `scanblocks` RPC call allows one to get relevant blockhashes from a set of descriptors by scanning all blockfilters in a given range.\r\n> \r\n> **Example:**\r\n> \r\n> `scanblocks start '[\"addr(<bitcoin_address>)\"]' 661000` (returns relevant blockhashes for `<bitcoin_address>` from blockrange 661000->tip)\r\n> \r\n> ## Why is this useful?\r\n> **Fast wallet rescans**: get the relevant blocks and only rescan those via `rescanblockchain getblockheader(<hash>)[height] getblockheader(<hash>)[height])`. A future PR may add an option to allow to provide an array of blockhashes to `rescanblockchain`.\r\n> \r\n> **prune wallet rescans**: (_needs additional changes_): together with a call to fetch blocks from the p2p network if they have been pruned, it would allow to rescan wallets back to the genesis block in pruned mode (relevant #15946).\r\n> \r\n> **SPV mode** (_needs additional changes_): it would be possible to build the blockfilterindex from the p2p network (rather then deriving them from the blocks) and thus allow some sort of hybrid-SPV mode with moderate bandwidth consumption (related #9483)\r\n\r\n\r\n",
  "closed_by": {
    "login": "achow101",
    "id": 3782274,
    "node_id": "MDQ6VXNlcjM3ODIyNzQ=",
    "avatar_url": "https://avatars.githubusercontent.com/u/3782274?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/achow101",
    "html_url": "https://github.com/achow101",
    "followers_url": "https://api.github.com/users/achow101/followers",
    "following_url": "https://api.github.com/users/achow101/following{/other_user}",
    "gists_url": "https://api.github.com/users/achow101/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/achow101/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/achow101/subscriptions",
    "organizations_url": "https://api.github.com/users/achow101/orgs",
    "repos_url": "https://api.github.com/users/achow101/repos",
    "events_url": "https://api.github.com/users/achow101/events{/privacy}",
    "received_events_url": "https://api.github.com/users/achow101/received_events",
    "type": "User",
    "site_admin": false
  },
  "reactions": {
    "url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549/reactions",
    "total_count": 8,
    "+1": 5,
    "-1": 0,
    "laugh": 0,
    "hooray": 0,
    "confused": 1,
    "heart": 0,
    "rocket": 2,
    "eyes": 0
  },
  "timeline_url": "https://api.github.com/repos/bitcoin/bitcoin/issues/23549/timeline",
  "performed_via_github_app": null,
  "state_reason": null
}
